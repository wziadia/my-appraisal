﻿using Employee_Appraisal.AIFEmployee;
using Employee_Appraisal.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Employee_Appraisal.Controllers
{
    public class AppraisalReportsController : Controller
    {
        private YIACOAPPSDBEntities db = new YIACOAPPSDBEntities();
        List<EmpReport> Query;
        List<EmpListReport> employeeList = new List<EmpListReport>();
        AxdEntity_EmplTable_1 employee;

        public class EmpReport
        {
            public int AppraisalId { get; set; }
            public string EmplId { get; set; }
            public string EmplName { get; set; }
            public string EmplOrganization { get; set; }
            public string EmplPosition { get; set; }
            public double ADMN { get; set; }
            public double COM { get; set; }
            public double SALES { get; set; }
            public double RESP { get; set; }
            public double TOTAL { get; set; }
        }

        public class DevReport
        {
            public int AppraisalId { get; set; }
            public string EmplId { get; set; }
            public string EmplName { get; set; }
            public string EmplOrganization { get; set; }
            public string EmplPosition { get; set; }
            public string DevelopmentNeeds { get; set; }
            public string DevelopmentNeedsText { get; set; }
            public string TargetDate { get; set; }
            public string ActionPlan { get; set; }
        }

        public class EmpListReport
        {
            public string EmplId { get; set; }
            public string EmplName { get; set; }
            public string EmplOrganization { get; set; }
            public string EmplPosition { get; set; }
            public DateTime JoinDate { get; set; }
            public string ManagerId { get; set; }
            public string ManagerName { get; set; }
        }

        public ActionResult Index(string login)
        {
            employee = AxdEntity_EmplTable_1.ConstructEmployee(login, "YIAC");
            if (employee == null)
                employee = AxdEntity_EmplTable_1.ConstructEmployee(login, "PAY");

            AppraisalAdmin appraisalAdmins = db.AppraisalAdmins.Find(employee.EmplId);

            return View(appraisalAdmins);
        }

        public ActionResult EmployeeList(string ORG)
        {
            ViewBag.ORGANIZATION = new SelectList(AxdEntity_EmplTable_1.ConstructEmployeeList().Select(st => st.OrganizationUnitName == null ? "None" : st.OrganizationUnitName).Distinct().OrderBy(OrganizationUnitName => OrganizationUnitName).ToList());
            if (ORG != null)
            {
                if (ORG != "Select all" && ORG != "")
                {

                    employeeList = new List<EmpListReport>();
                    employeeList = (from emp in (AxdEntity_EmplTable_1.ConstructEmployeeList().Where(st => st.OrganizationUnitName == ORG))
                                    select new EmpListReport()
                                    {
                                        EmplId = emp.EmplId,
                                        EmplName = emp.Name,
                                        EmplOrganization = emp.OrganizationUnitName,
                                        EmplPosition = emp.Title,
                                        JoinDate = emp.JoinDate,
                                        ManagerId = (emp.Manager == null ? "" : emp.Manager.EmplId),
                                        ManagerName = (emp.Manager == null ? "" : emp.Manager.Name),
                                    }).ToList();

                    ViewData["EmpList"] = employeeList;
                    return View();
                }
                else
                {

                    employeeList = new List<EmpListReport>();
                    employeeList = (from emp in (AxdEntity_EmplTable_1.ConstructEmployeeList())
                                    select new EmpListReport()
                                    {
                                        EmplId = emp.EmplId,
                                        EmplName = emp.Name,
                                        EmplOrganization = emp.OrganizationUnitName,
                                        EmplPosition = emp.Title,
                                        JoinDate = emp.JoinDate,
                                        ManagerId = (emp.Manager == null ? "" : emp.Manager.EmplId),
                                        ManagerName = (emp.Manager == null ? "" : emp.Manager.Name),
                                    }).ToList();

                    ViewData["EmpList"] = employeeList;
                    return View();

                }
            }
            return View();
        }

        public ActionResult ObjAppStatusRpt()
        {
            return View();
        }

        public ActionResult TrainRpt(string ORG, int YR = 0)
        {
            ViewBag.ORGANIZATION = new SelectList(AxdEntity_EmplTable_1.ConstructEmployeeList().Select(st => st.OrganizationUnitName == null ? "None" : st.OrganizationUnitName).Distinct().OrderBy(OrganizationUnitName => OrganizationUnitName).ToList());
            ViewBag.YEAR = new SelectList(db.AppraisalStatusYears.Select(st => st.Year).Distinct().OrderBy(Year => Year).ToList());
            DateTime targetDate = Convert.ToDateTime((YR + 1) + "-01-01");

            if (ORG != null)
            {
                if (ORG != "Select all" && ORG != "")
                {
                    using (YIACOAPPSDBEntities dbContext = new YIACOAPPSDBEntities())
                    {
                        List<DevReport> DevQuery = new List<DevReport>();

                        DevQuery = (from emp in (AxdEntity_EmplTable_1.ConstructEmployeeList().Where(st => st.OrganizationUnitName == ORG))
                                    join APP in db.Appraisals.Where(year => year.Year == YR)
                                     on emp.EmplId equals APP.EmplId
                                    join APD in db.AppraisalDevelopmentPlans
                                       on APP.AppraisalId equals APD.AppraisalId
                                    where APD.TargetDate >= targetDate
                                    select new DevReport()
                                    {
                                        AppraisalId = APD.AppraisalId,
                                        EmplId = emp.EmplId,
                                        EmplName = emp.Name,
                                        EmplOrganization = emp.OrganizationUnitName,
                                        EmplPosition = emp.TitleNameEN,
                                        DevelopmentNeeds = APD.DevelopmentNeeds,
                                        DevelopmentNeedsText = APD.DevelopmentNeedsText,
                                        TargetDate = APD.TargetDate.ToString(),
                                        ActionPlan = APD.ActionPlan,
                                   }).ToList();

                        ViewData["DevRpt"] = DevQuery;
                        return View();
                    }
                }
                else
                {
                    using (YIACOAPPSDBEntities dbContext = new YIACOAPPSDBEntities())
                    {
                        List<DevReport> DevQuery = new List<DevReport>();
                        
                        DevQuery = (from emp in (AxdEntity_EmplTable_1.ConstructEmployeeList())
                                    join APP in db.Appraisals.Where(year => year.Year == YR)
                                       on emp.EmplId equals APP.EmplId
                                    join APD in db.AppraisalDevelopmentPlans
                                       on APP.AppraisalId equals APD.AppraisalId
                                       where APD.TargetDate >= targetDate
                                    select new DevReport()
                                    {
                                        AppraisalId = APD.AppraisalId,
                                        EmplId = emp.EmplId,
                                        EmplName = emp.Name,
                                        EmplOrganization = emp.OrganizationUnitName,
                                        EmplPosition = emp.TitleNameEN,
                                        DevelopmentNeeds = APD.DevelopmentNeeds,
                                        DevelopmentNeedsText = APD.DevelopmentNeedsText,
                                        TargetDate = APD.TargetDate.ToString(),
                                        ActionPlan = APD.ActionPlan,
                                    }).ToList();

                        ViewData["DevRpt"] = DevQuery;
                        return View();
                    }
                }
            }
            else return View();
        }

        public ActionResult AppRpt(string ORG, int YR = 0)
        {
            ViewBag.ORGANIZATION = new SelectList(AxdEntity_EmplTable_1.ConstructEmployeeList().Select(st => st.OrganizationUnitName == null ? "None" : st.OrganizationUnitName).Distinct().OrderBy(OrganizationUnitName => OrganizationUnitName).ToList());
            ViewBag.YEAR = new SelectList(db.AppraisalStatusYears.Select(st => st.Year).Distinct().OrderBy(Year => Year).ToList());
            if (ORG != null)
            {
                if (ORG != "Select all" && ORG != "")
                {
                    using (YIACOAPPSDBEntities dbContext = new YIACOAPPSDBEntities())
                    {
                        Query = new List<EmpReport>();

                        Query = (from emp in (AxdEntity_EmplTable_1.ConstructEmployeeList().Where(st => st.OrganizationUnitName == ORG))
                                 join APP in db.Appraisals.Where(year => year.Year == YR)
                                    on emp.EmplId equals APP.EmplId
                                 join APE in db.AppraisalEmployees on APP.AppraisalId equals APE.AppraisalId
                                 into joined
                                 select new EmpReport()
                                {
                                    AppraisalId = joined.Select(AppId => AppId.AppraisalId).FirstOrDefault(),
                                    EmplId = emp.EmplId,
                                    EmplName = emp.Name,
                                    EmplOrganization = emp.OrganizationUnitName,
                                    EmplPosition = emp.TitleNameEN,
                                    ADMN = Math.Round(joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "RESP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score), 1),
                                    COM = Math.Round(joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMA" || qg.AppraisalQuestion.QuestionGroup == "COME" || qg.AppraisalQuestion.QuestionGroup == "COML" || qg.AppraisalQuestion.QuestionGroup == "COMS").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score), 1),
                                    SALES = Math.Round(joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "SLST").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score), 1),
                                    RESP = Math.Round(joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score), 1),
                                    TOTAL = Math.Round((APP.FinancialGoal == 1 ? ((
                                            (joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "SLST").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) * 6) +
                                            (joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "RESP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) * 2) +
                                            joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) +
                                            joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMA" || qg.AppraisalQuestion.QuestionGroup == "COME" || qg.AppraisalQuestion.QuestionGroup == "COML" || qg.AppraisalQuestion.QuestionGroup == "COMS").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score)) / 10) :
                                            (((joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "RESP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) * 6) +
                                            (joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) * 2) +
                                            (joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMA" || qg.AppraisalQuestion.QuestionGroup == "COME" || qg.AppraisalQuestion.QuestionGroup == "COML" || qg.AppraisalQuestion.QuestionGroup == "COMS").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) * 2)) / 10)), 1),
                                }).ToList();

                        ViewData["AppRpt"] = Query;
                        return View();
                    }
                }
                else
                {
                    using (YIACOAPPSDBEntities dbContext = new YIACOAPPSDBEntities())
                    {
                        Query = (from emp in (AxdEntity_EmplTable_1.ConstructEmployeeList())
                                 join APP in db.Appraisals.Where(year => year.Year == YR)
                                    on emp.EmplId equals APP.EmplId
                                 join APE in db.AppraisalEmployees on APP.AppraisalId equals APE.AppraisalId
                                 into joined
                                 select new EmpReport()
                                 {
                                     AppraisalId = joined.Select(AppId => AppId.AppraisalId).FirstOrDefault(),
                                     EmplId = joined.Select(EmpId => EmpId.EmplId).FirstOrDefault(),
                                     EmplName = emp.Name,
                                     EmplOrganization = emp.OrganizationUnitName,
                                     EmplPosition = emp.TitleNameEN,
                                     ADMN = Math.Round(joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "RESP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score), 1),
                                     COM = Math.Round(joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMA" || qg.AppraisalQuestion.QuestionGroup == "COME" || qg.AppraisalQuestion.QuestionGroup == "COML" || qg.AppraisalQuestion.QuestionGroup == "COMS").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score), 1),
                                     SALES = Math.Round(joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "SLST").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score), 1),
                                     RESP = Math.Round(joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score), 1),
                                     TOTAL = Math.Round((APP.FinancialGoal == 1 ? ((
                                            (joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "SLST").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) * 6) +
                                            (joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "RESP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) * 2) +
                                             joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) +
                                             joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMA" || qg.AppraisalQuestion.QuestionGroup == "COME" || qg.AppraisalQuestion.QuestionGroup == "COML" || qg.AppraisalQuestion.QuestionGroup == "COMS").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score)) / 10) :
                                             (((joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "RESP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) * 6) +
                                             (joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMP").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) * 2) +
                                             (joined.Where(qg => qg.AppraisalQuestion.QuestionGroup == "COMA" || qg.AppraisalQuestion.QuestionGroup == "COME" || qg.AppraisalQuestion.QuestionGroup == "COML" || qg.AppraisalQuestion.QuestionGroup == "COMS").DefaultIfEmpty().Average(sl => sl == null ? 0 : sl.Score) * 2)) / 10)), 1),
                                 }).ToList();

                        ViewData["AppRpt"] = Query;
                        return View();
                    }
                }
            }
            else return View();
        }

        public ActionResult EmplAppRpt(string emplId, int YR = 0)
        {
            ViewBag.YEAR = new SelectList(db.AppraisalStatusYears.Select(st => st.Year).Distinct().OrderBy(Year => Year).ToList());
            if (emplId != null)
            {
                using (YIACOAPPSDBEntities dbContext = new YIACOAPPSDBEntities())
                {
                    Query = (from emp in (AxdEntity_EmplTable_1.ConstructEmployeeList().Where(st => st.EmplId == emplId))
                             join APP in db.Appraisals.Where(year => year.Year == YR)
                                on emp.EmplId equals APP.EmplId
                                into joined
                             select new EmpReport()
                            {
                                AppraisalId = joined.Select(AppId => AppId.AppraisalId).FirstOrDefault(),
                                EmplId = joined.Select(EmpId => EmpId.EmplId).FirstOrDefault(),
                                EmplName = emp.Name,
                                EmplOrganization = emp.OrganizationUnitName,
                                EmplPosition = emp.TitleNameEN
                            }
                             ).ToList();

                    ViewData["EmplAppRpt"] = Query;
                    return View();
                }
            }
            else
            {
                ViewData["EmplAppRpt"] = Query;
                return View();
            }
        }
    }
}