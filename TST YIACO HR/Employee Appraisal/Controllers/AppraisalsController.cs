﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Employee_Appraisal.Models;
using System.Data.Entity.Validation;
using Employee_Appraisal.AIFEmployee;
using Employee_Appraisal.AIFReportingRelationships;
using System.Diagnostics;
using Microsoft.Win32;

namespace Employee_Appraisal.Controllers
{
    public class CompetenciesToUpdate
    {
        public int AppraisalId { get; set; }
        public int QuestionId { get; set; }
        public string FieldValue { get; set; }
        public string ColumnToUpdate { get; set; }
        public string EmplID { get; set; }
    }

    public class AppraisalsController : Controller
    {
        private YIACOAPPSDBEntities db = new YIACOAPPSDBEntities();
        AxdEntity_EmplTable_1 employee;


        // GET: Appraisals
        public ActionResult Index()
        {
            employee = AxdEntity_EmplTable_1.ConstructEmployee(this.User.Identity.Name, "YIAC");

            if (employee == null)
                employee = AxdEntity_EmplTable_1.ConstructEmployee(this.User.Identity.Name, "PAY");

            var appraisal = db.Appraisals.Where(a => a.EmplId == employee.EmplId && (a.Deleted == 0 || a.Deleted == null));

            return View(appraisal.ToList());
        }

        // GET: Appraisals/Details/5
        public ActionResult Details(int? id, string Delts = null)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appraisal appraisal = db.Appraisals.Find(id);
            if (appraisal == null)
            {
                return HttpNotFound();
            }
            if (Delts != null)
            {
                ViewBag.Delts = "YES";
            }

            AppraisalLog appraisalLog = new AppraisalLog();

            appraisalLog.AppraisalId = appraisal.AppraisalId;
            appraisalLog.Appraisal = appraisal;
            if (appraisal.Stage == 1)
                appraisalLog.LogDescription = "Objectives Last Viewed.";
            else if (appraisal.Stage == 5)
                appraisalLog.LogDescription = "Appraisal Last Viewed.";
            appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
            appraisalLog.LogAt = DateTime.Now;

            db.AppraisalLogs.Add(appraisalLog);

            return View(appraisal);
        }

        // GET: Appraisals/Create
        public ActionResult Create(string id)
        {
            if (DateTime.Now.Month == 1)
                ViewBag.Year = new SelectList(db.AppraisalStatusYears.Where(st => st.Status == 1).ToList(), "Year", "Year", (DateTime.Now.Year - 1));
            else
                ViewBag.Year = new SelectList(db.AppraisalStatusYears.Where(st => st.Status == 1).ToList(), "Year", "Year", DateTime.Now.Year);
            ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies.ToList(), "JobFamilyID", "JobFamilyName");
            ViewBag.EmplID = id;

            employee = AxdEntity_EmplTable_1.ConstructEmployee(this.User.Identity.Name, "YIAC");

            if (employee.EmplId == null)
                employee = AxdEntity_EmplTable_1.ConstructEmployee(this.User.Identity.Name, "PAY");

            ViewBag.AppraisarId = employee.EmplId;
            string emplName = "";
            emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(id, "YIAC").Name;
            if (emplName == null)
                emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(id, "PAY").Name;

            ViewBag.name = emplName;

            return View();
        }

        // POST: Appraisals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AppraisalId,EmplId,AppraisalDate,Year,AppraisalRemarks,AppraisalFeedback,KPIFeedback,AppraiserId,EmplGroupId,FinancialGoal,Status,JobFamily,AcceptanceAppraisalDate,AcceptanceObjectivesDate")] Appraisal appraisal)
        {
            if (ModelState.IsValid)
            {
                Appraisal existAppraisal = db.Appraisals.Where(x => x.EmplId == appraisal.EmplId && x.Year == appraisal.Year && (x.Deleted == 0 || x.Deleted == null)).FirstOrDefault();
                if (existAppraisal == null)
                {
                    db.Appraisals.Add(appraisal);
                    db.SaveChanges();

                    AppraisalEmployee appraisalEmployee = new AppraisalEmployee();
                    for (int i = 1; i <= 5; i++)
                    {
                        appraisalEmployee.AppraisalId = appraisal.AppraisalId;
                        appraisalEmployee.EmplId = appraisal.EmplId;
                        appraisalEmployee.QuestionWeight = 20;
                        appraisalEmployee.QuestionCode = "C0" + i;
                        if (i == 1)
                            appraisalEmployee.QuestionText = "Clearly conveying information and ideas through a variety of media to individuals or groups in a manner that engages the audience and helps them understand and retain the message";
                        if (i == 2)
                            appraisalEmployee.QuestionText = "Taking prompt action to accomplish objectives; taking action to achieve goals beyond what is required; being proactive";
                        if (i == 3)
                            appraisalEmployee.QuestionText = "Making customers and their needs a primary focus of one’s actions; developing and sustaining productive customer relationships";
                        if (i == 4)
                            appraisalEmployee.QuestionText = "Accomplishing tasks by considering all areas involved, no matter how small; showing concern for all aspects of the job; accurately checking processes and tasks; being watchful over a period of time";
                        if (i == 5)
                            appraisalEmployee.QuestionText = "Adheres to the team’s expectations and guidelines; fulfills team responsibilities; demonstrates personal commitment to the team";

                        db.AppraisalEmployees.Add(appraisalEmployee);
                        db.SaveChanges();
                    }

                    AppraisalLog appraisalLog = new AppraisalLog();

                    appraisalLog.AppraisalId = appraisal.AppraisalId;
                    appraisalLog.Appraisal = appraisal;
                    appraisalLog.LogDescription = "Appraisal Created.";
                    appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                    appraisalLog.LogAt = DateTime.Now;

                    db.AppraisalLogs.Add(appraisalLog);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }

                    return Redirect("~/Employees/Appraisals/" + appraisal.EmplId + "?name=" + appraisal.Employee.Name);
                }
                ViewBag.Error = "Can not create another appraisal for the same year to this employee.";
                ViewBag.Year = new SelectList(db.AppraisalStatusYears, "Year", "Year", appraisal.Year);
                ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies, "JobFamilyID", "JobFamilyName", appraisal.JobFamily);
                ViewBag.EmplID = appraisal.EmplId;
                employee = AxdEntity_EmplTable_1.ConstructEmployee(this.User.Identity.Name, "YIAC");
                if (employee.EmplId == null)
                    employee = AxdEntity_EmplTable_1.ConstructEmployee(this.User.Identity.Name, "PAY");
                ViewBag.AppraisarId = employee.EmplId;
                return View();
            }

            ViewBag.Year = new SelectList(db.AppraisalStatusYears, "Year", "Year", appraisal.Year);
            ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies.ToList(), "JobFamilyID", "JobFamilyName");
            return View(appraisal);
        }

        // GET: Appraisals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appraisal appraisal = db.Appraisals.Find(id);
            if (appraisal == null)
            {
                return HttpNotFound();
            }
            ViewBag.Year = new SelectList(db.AppraisalStatusYears, "Year", "Year", appraisal.Year);
            ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies, "JobFamilyID", "JobFamilyName", appraisal.JobFamily);
            return View(appraisal);
        }

        // POST: Appraisals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AppraisalId,EmplId,AppraisalDate,Year,AppraisalRemarks,AppraisalFeedback,KPIFeedback,AppraiserId,EmplGroupId,FinancialGoal,Status,JobFamily,AcceptanceAppraisalDate,AcceptanceObjectivesDate")] Appraisal appraisal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(appraisal).State = EntityState.Modified;

                AppraisalLog appraisalLog = new AppraisalLog();

                appraisalLog.AppraisalId = appraisal.AppraisalId;
                appraisalLog.Appraisal = appraisal;
                appraisalLog.LogDescription = "Appraisal Edited.";
                appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                appraisalLog.LogAt = DateTime.Now;

                db.AppraisalLogs.Add(appraisalLog);

                db.SaveChanges();
                return Redirect("~/Employees/Appraisals/" + appraisal.EmplId + "?name=" + appraisal.Employee.Name);
            }
            ViewBag.Year = new SelectList(db.AppraisalStatusYears, "Year", "Year", appraisal.Year);
            ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies, "JobFamilyID", "JobFamilyName", appraisal.JobFamily);
            return View(appraisal);
        }

        // GET: Appraisals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appraisal appraisal = db.Appraisals.Find(id);
            if (appraisal == null)
            {
                return HttpNotFound();
            }
            if (appraisal.Status > 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "A processed appraisal cannot be deleted.");
            }
            return View(appraisal);
        }

        // POST: Appraisals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Appraisal appraisal = db.Appraisals.Find(id);

            //List<AppraisalEmployee> emplList = (from ape in db.AppraisalEmployees
            //                                    join ap in db.Appraisals on ape.AppraisalId equals ap.AppraisalId
            //                                    where ape.AppraisalId == appraisal.AppraisalId
            //                                    select ape).ToList();

            //List<AppraisalDevelopmentPlan> devList = (from ape in db.AppraisalDevelopmentPlans
            //                                          join ap in db.Appraisals on ape.AppraisalId equals ap.AppraisalId
            //                                          where ape.AppraisalId == appraisal.AppraisalId
            //                                          select ape).ToList();

            //foreach (AppraisalEmployee appraisalEmployee in emplList)
            //{
            //    db.AppraisalEmployees.Remove(appraisalEmployee);
            //}

            //foreach (AppraisalDevelopmentPlan appraisalDevelopmentPlan in devList)
            //{
            //    db.AppraisalDevelopmentPlans.Remove(appraisalDevelopmentPlan);
            //}

            //db.SaveChanges();

            appraisal.Deleted = 1;// db.Appraisals.Remove(appraisal);
            db.Entry(appraisal).State = EntityState.Modified;

            AppraisalLog appraisalLog = new AppraisalLog();

            appraisalLog.AppraisalId = appraisal.AppraisalId;
            appraisalLog.Appraisal = appraisal;
            appraisalLog.LogDescription = "Appraisal Deleted.";
            appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
            appraisalLog.LogAt = DateTime.Now;

            db.AppraisalLogs.Add(appraisalLog);

            db.SaveChanges();
            return Redirect("~/Employees/Appraisals/" + appraisal.EmplId + "?name=" + appraisal.Employee.Name);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult UpdateObjectivesAcceptance(int id, int accept, string feedback)
        {
            Appraisal appraisal = db.Appraisals.Find(id);
            employee = AxdEntity_EmplTable_1.ConstructEmployee(appraisal.EmplId, "YIAC");
            if (employee == null)
                employee = AxdEntity_EmplTable_1.ConstructEmployee(appraisal.EmplId, "PAY");

            if (ModelState.IsValid)
            {
                appraisal.Stage = accept;
                appraisal.KPIFeedback = feedback;

                db.Entry(appraisal).State = EntityState.Modified;

                AppraisalLog appraisalLog = new AppraisalLog();

                appraisalLog.AppraisalId = appraisal.AppraisalId;
                appraisalLog.Appraisal = appraisal;
                if (accept == 2)
                    appraisalLog.LogDescription = "Objectives Accepted.";
                else if (accept == 3)
                    appraisalLog.LogDescription = "Objectives Rejected.";
                appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                appraisalLog.LogAt = DateTime.Now;

                db.AppraisalLogs.Add(appraisalLog);

                db.SaveChanges();

                string[] lines = { "FROM: itd@yiaco.com",
                                 "TO: " + employee.Manager.Email,
                                 "SUBJECT: " + employee.Name + " Has responced to the Employee Performance Objectives Request for the year" + appraisal.Year,
                                 "",
                                 "Dear Mr./Mrs."+ employee.Manager.Name +",",
                                 "",
                                 "Please click the below link to review my respond to the objectives for the year" + appraisal.Year + ".",
                                 "",
                                 "http://yiacoinet01:8016/Appraisal",
                                 "",
                                 "After logging in, click on 'My Employees', then click on 'Details' beside my name, then click on 'Details' beside this year's appraisal '"+ appraisal.Year +"', Scroll down to the bottom to review the comments on my Objectives.",
                                 "",
                                 "Thanks & Best Regards,",
                                 employee.Manager.Name
                             };

                //Process P = Process.Start(String.Format("mailto:{0}?subject={1}&body={2}", "w.ziadia@yiaco.com", "WalaaElzayadia" + " Has responced to the Employee Performance Objectives Request for the year" + appraisal.Year,
                //    "Dear Mr./Mrs." + System.Environment.NewLine + "Johnson Joes" + "," + System.Environment.NewLine +
                //                 "Please click the below link to review my respond to the objectives for the year" + appraisal.Year + "." +
                //                 System.Environment.NewLine +
                //                 "http://yiacoinet01:8016/Appraisal" +
                //                 System.Environment.NewLine +
                //                 "After logging in, click on 'My Employees', then click on 'Details' beside my name, then click on 'Details' beside this year's appraisal '" + appraisal.Year + "', Scroll down to the bottom to review the comments on my Objectives." +
                //                System.Environment.NewLine +
                //                 "Thanks & Best Regards," +
                //                  "Johnson Joes"));
                //P.WaitForExit(10);
                //P.WaitForExit();
                //int result = P.ExitCode;

                //sendMail("w.ziadia@yiaco.com", "Test");


                System.IO.File.WriteAllLines(@"C:\inetpub\mailroot\Pickup\WriteLines.txt", lines);
                //System.IO.File.WriteAllLines(@"C:\CopyTest\WriteLines.txt", lines);

            }
            ViewBag.Year = new SelectList(db.AppraisalStatusYears, "Year", "Year", appraisal.Year);
            ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies, "JobFamilyID", "JobFamilyName", appraisal.JobFamily);
            return RedirectToAction("Details/" + appraisal.AppraisalId.ToString(), "Appraisals", null);
        }

        public ActionResult UpdateAppraisalAcceptance(int id, int accept, string feedback)
        {
            Appraisal appraisal = db.Appraisals.Find(id);

            employee = AxdEntity_EmplTable_1.ConstructEmployeeById(appraisal.EmplId, "YIAC");
            if (employee == null)
                employee = AxdEntity_EmplTable_1.ConstructEmployeeById(appraisal.EmplId, "PAY");

            if (ModelState.IsValid)
            {
                appraisal.Stage = accept;
                appraisal.AppraisalFeedback = feedback;

                db.Entry(appraisal).State = EntityState.Modified;

                AppraisalLog appraisalLog = new AppraisalLog();

                appraisalLog.AppraisalId = appraisal.AppraisalId;
                appraisalLog.Appraisal = appraisal;
                if (accept == 6)
                    appraisalLog.LogDescription = "Appraisal Accepted.";
                else if (accept == 7)
                    appraisalLog.LogDescription = "Appraisal Rejected.";
                appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                appraisalLog.LogAt = DateTime.Now;

                db.AppraisalLogs.Add(appraisalLog);

                db.SaveChanges();

                string[] lines = { "FROM: itd@yiaco.com",
                                 "TO: " + employee.Manager.Email,
                                 "SUBJECT: " + employee.Name + " Has responced to the Employee Performance Appraisal Request for the year" + appraisal.Year,
                                 "",
                                 "Dear Mr./Mrs."+ employee.Manager.Name +",",
                                 "",
                                 "Please click the below link to review my respond to the Appraisal for the year" + appraisal.Year + ".",
                                 "",
                                 "http://yiacoinet01:8016/Appraisal",
                                 "",
                                 "After logging in, click on 'My Employees', then click on 'Details' beside my name, then click on 'Details' beside this year's appraisal '"+ appraisal.Year +"', Scroll down to the bottom to review the comments on my Appraisal.",
                                 "",
                                 "Thanks & Best Regards,",
                                 employee.Manager.Name
                             };
                System.IO.File.WriteAllLines(@"C:\inetpub\mailroot\Pickup\WriteLines.txt", lines);
                //System.IO.File.WriteAllLines(@"C:\CopyTest\WriteLines.txt", lines);
            }
            ViewBag.Year = new SelectList(db.AppraisalStatusYears, "Year", "Year", appraisal.Year);
            ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies, "JobFamilyID", "JobFamilyName", appraisal.JobFamily);
            return RedirectToAction("Details/" + appraisal.AppraisalId.ToString(), "Appraisals", null);
        }

        public ActionResult SaveComment(int id, string feedback, int accept)
        {
            Appraisal appraisal = db.Appraisals.Find(id);

            if (ModelState.IsValid)
            {
                if (accept == 1)
                    appraisal.KPIFeedback = feedback;
                if (accept == 4)
                    appraisal.AppraisalRemarks = feedback;
                if (accept == 5)
                    appraisal.AppraisalFeedback = feedback;
                db.Entry(appraisal).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
            return RedirectToAction("Details/" + appraisal.AppraisalId.ToString(), "Appraisals", null);
        }

        private void sendMail(String mail, String description)
        {

            RegistryKey key = Registry.ClassesRoot;
            RegistryKey subKey = key.OpenSubKey("Outlook.Application");

            if (subKey != null)
            {


                if ((Process.GetProcessesByName("Outlook").Length == 0) && (Process.GetProcessesByName("Outlook.exe").Length == 0))
                {
                    System.Diagnostics.Process.Start("Outlook");
                }


                Microsoft.Office.Interop.Outlook.Application app = new Microsoft.Office.Interop.Outlook.Application();
                Microsoft.Office.Interop.Outlook.MailItem mailItem = app.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);
                mailItem.Subject = "Release Notice";
                mailItem.To = mail;

                String bodyMessage = description;

                mailItem.Body = bodyMessage;

                mailItem.Display(false);
                mailItem.Send();

            }
        }
    }
}
