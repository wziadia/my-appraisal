﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Employee_Appraisal.Models;

namespace Employee_Appraisal.Controllers
{
    public class AppraisalStatusController : Controller
    {
        private YIACOAPPSDBEntities db = new YIACOAPPSDBEntities();

        // GET: /AppraisalStatus/
        public ActionResult Index()
        {
            return View(db.AppraisalStatusYears.ToList());
        }

        // GET: /AppraisalStatus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppraisalStatusYear appraisalstatus = db.AppraisalStatusYears.Find(id);
            if (appraisalstatus == null)
            {
                return HttpNotFound();
            }
            return View(appraisalstatus);
        }

        // GET: /AppraisalStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /AppraisalStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Year,Status")] AppraisalStatusYear appraisalstatus)
        {
            if (ModelState.IsValid)
            {
                db.AppraisalStatusYears.Add(appraisalstatus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(appraisalstatus);
        }

        // GET: /AppraisalStatus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppraisalStatusYear appraisalstatus = db.AppraisalStatusYears.Find(id);
            if (appraisalstatus == null)
            {
                return HttpNotFound();
            }
            return View(appraisalstatus);
        }

        // POST: /AppraisalStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Year,Status")] AppraisalStatusYear appraisalstatus)
        {
            if (ModelState.IsValid)
            {
                List<Appraisal> appraisals = db.Appraisals.Where(yr => yr.Year == appraisalstatus.Year).ToList();
                int newStatus;

                if (appraisalstatus.Status == 0)
                    newStatus = 1;
                else
                    newStatus = 0;

                for(int i = 0; i < appraisals.Count(); i++)
                {
                    appraisals[i].Status = newStatus;
                }

                foreach (var item in appraisals)
                {
                    db.Entry(item).State = EntityState.Modified;
                }                

                db.Entry(appraisalstatus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(appraisalstatus);
        }

        // GET: /AppraisalStatus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppraisalStatusYear appraisalstatus = db.AppraisalStatusYears.Find(id);
            if (appraisalstatus == null)
            {
                return HttpNotFound();
            }
            return View(appraisalstatus);
        }

        // POST: /AppraisalStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AppraisalStatusYear appraisalstatus = db.AppraisalStatusYears.Find(id);
            db.AppraisalStatusYears.Remove(appraisalstatus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
