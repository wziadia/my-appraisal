﻿using Employee_Appraisal.AIFEmployee;
using Employee_Appraisal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Employee_Appraisal.Controllers
{
    public class AppraisalDevelopmentPlanController : Controller
    {
        private YIACOAPPSDBEntities db = new YIACOAPPSDBEntities();

        // GET: /AppraisalDevelopmentPlan/
        public ActionResult Index(int appraisalId, string name)
        {
            var development = db.AppraisalDevelopmentPlans.Where(a => a.AppraisalId == appraisalId);

            return View(development.ToList());
        }

        //
        // GET: /AppraisalDevelopmentPlan/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /AppraisalDevelopmentPlan/Create
        public ActionResult Create(int AppraisalId, string DevelopmentNeedsCode, string selVal)
        {
            string emplName = "";

            ViewBag.NeedsCode = db.AppraisalQuestionGroups.Where(name => name.Name == DevelopmentNeedsCode).Select(l => l.GroupId).FirstOrDefault().ToString();

            selVal = ViewBag.NeedsCode;
            ViewBag.DevelopmentNeedsCode = new SelectList(db.AppraisalQuestionGroups.Where(typ => typ.TypeId == 5)
            .ToList(), "GroupId", "Name", DevelopmentNeedsCode);
            ViewBag.DevelopmentNeedsText = new SelectList(db.AppraisalQuestions
                .Where(desc => desc.QuestionGroup == selVal)
                .ToList(), "QuestionCode", "QuestionText");
            ViewBag.AppraisalId = AppraisalId;
            ViewBag.SelVal = selVal;
            ViewBag.DEVTXT = DevelopmentNeedsCode;
            emplName = "";
            emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "YIAC").Name;
            if (emplName == null)
                emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "PAY").Name;

            ViewBag.name = emplName;

            return View();
        }

        //
        // POST: /AppraisalDevelopmentPlan/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AppraisalId,DevelopmentNeeds,DevelopmentNeedsText,TargetDate,ActionPlan")] AppraisalDevelopmentPlan appraisalDevelopmentPlan, string DevelopmentNeedsCode, string selVal)
        {
            string emplName = "";
            if (ModelState.IsValid)
            {
                try
                {
                    db.AppraisalDevelopmentPlans.Add(appraisalDevelopmentPlan);
                    db.SaveChanges();

                    emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "YIAC").Name;
                    if (emplName == null)
                        emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "PAY").Name;

                    return Redirect("~/Employees/Details/" + appraisalDevelopmentPlan.AppraisalId);
                }
                catch (Exception ex)
                {
                    return View();
                }
            }


            ViewBag.DevelopmentNeedsCode = new SelectList(db.AppraisalQuestionGroups.Where(typ => typ.TypeId == 5)
                .ToList(), "GroupId", "Name", DevelopmentNeedsCode);
            ViewBag.DevelopmentNeedsText = new SelectList(db.AppraisalQuestions
                .Where(desc => desc.QuestionGroup == DevelopmentNeedsCode)
                .ToList(), "QuestionCode", "QuestionText");
            ViewBag.AppraisalId = appraisalDevelopmentPlan.AppraisalId;
            ViewBag.SelVal = selVal;
            emplName = "";
            emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "YIAC").Name;
            if (emplName == null)
                emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "PAY").Name;

            ViewBag.name = emplName;
            return View();
        }

        //
        // GET: /AppraisalDevelopmentPlan/Edit/5
        public ActionResult Edit(int? id, string DevelopmentNeedsCode, string selVal)
        {
            string emplName;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppraisalDevelopmentPlan appraisalDevelopmentPlan = db.AppraisalDevelopmentPlans.Find(id);
            if (appraisalDevelopmentPlan == null)
            {
                return HttpNotFound();
            }

            ViewBag.NeedsCode = db.AppraisalQuestionGroups.Where(name => name.Name == DevelopmentNeedsCode).Select(l => l.GroupId).FirstOrDefault().ToString();

            try
            {
                ViewBag.NeedsTxtCode = db.AppraisalQuestions.Where(name => name.QuestionText == appraisalDevelopmentPlan.DevelopmentNeedsText).Select(l => l.QuestionCode).FirstOrDefault().ToString();
            }
            catch (Exception ex)
            {
                ViewBag.NeedsTxtCode = db.AppraisalQuestions.Where(name => name.QuestionText == "Others").Select(l => l.QuestionCode).FirstOrDefault().ToString();
                ViewBag.NeedsTxt = appraisalDevelopmentPlan.DevelopmentNeedsText;
            }

            selVal = ViewBag.NeedsCode;

            ViewBag.DevelopmentNeedsCode = new SelectList(db.AppraisalQuestionGroups.Where(typ => typ.TypeId == 5)
            .ToList(), "GroupId", "Name", ViewBag.NeedsCode);
            ViewBag.DevelopmentNeedsText = new SelectList(db.AppraisalQuestions
                .Where(desc => desc.QuestionGroup == selVal)
                .ToList(), "QuestionCode", "QuestionText", ViewBag.NeedsTxtCode);

            ViewBag.AppraisalId = appraisalDevelopmentPlan.AppraisalId;
            ViewBag.SelVal = selVal;
            ViewBag.DEVTXT = DevelopmentNeedsCode;

            emplName = "";
            emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "YIAC").Name;
            if (emplName == null)
                emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "PAY").Name;

            ViewBag.name = emplName;
            return View(appraisalDevelopmentPlan);
        }

        //
        // POST: /AppraisalDevelopmentPlan/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DevelopmentPlanId,AppraisalId,DevelopmentNeeds,DevelopmentNeedsText,TargetDate,ActionPlan")] AppraisalDevelopmentPlan appraisalDevelopmentPlan, string DevelopmentNeedsCode, string selVal)
        {
            string emplName;
            if (ModelState.IsValid)
            {
                db.Entry(appraisalDevelopmentPlan).State = EntityState.Modified;
                db.SaveChanges();
                emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "YIAC").Name;
                if (emplName == null)
                    emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "PAY").Name;
                return Redirect("~/Employees/Details/" + appraisalDevelopmentPlan.AppraisalId);
            }
            ViewBag.DevelopmentNeedsCode = new SelectList(db.AppraisalQuestionGroups.Where(typ => typ.TypeId == 5)
                .ToList(), "GroupId", "Name", DevelopmentNeedsCode);
            ViewBag.DevelopmentNeedsText = new SelectList(db.AppraisalQuestions
                .Where(desc => desc.QuestionGroup == DevelopmentNeedsCode)
                .ToList(), "QuestionCode", "QuestionText");
            ViewBag.AppraisalId = appraisalDevelopmentPlan.AppraisalId;
            ViewBag.SelVal = selVal;
            emplName = "";
            emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "YIAC").Name;
            if (emplName == null)
                emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "PAY").Name;

            ViewBag.name = emplName;
            return View(appraisalDevelopmentPlan);
        }

        //
        // GET: /AppraisalDevelopmentPlan/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppraisalDevelopmentPlan appraisalDevelopmentPlan = db.AppraisalDevelopmentPlans.Find(id);
            if (appraisalDevelopmentPlan == null)
            {
                return HttpNotFound();
            }
            ViewBag.AppraisalId = appraisalDevelopmentPlan.AppraisalId;
            string emplName = "";
            emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "YIAC").Name;
            if (emplName == null)
                emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "PAY").Name;

            ViewBag.name = emplName;
            return View(appraisalDevelopmentPlan);
        }

        //
        // POST: /AppraisalDevelopmentPlan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppraisalDevelopmentPlan appraisalDevelopmentPlan = db.AppraisalDevelopmentPlans.Find(id);
            if (appraisalDevelopmentPlan == null)
            {
                return HttpNotFound();
            }

            db.AppraisalDevelopmentPlans.Remove(appraisalDevelopmentPlan);
            db.SaveChanges();
            string emplName = "";
            emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "YIAC").Name;
            if (emplName == null)
                emplName = AxdEntity_EmplTable_1.ConstructEmployeeById(db.Appraisals.Where(ap => ap.AppraisalId == appraisalDevelopmentPlan.AppraisalId).Select(l => l.EmplId).FirstOrDefault(), "PAY").Name;
            return Redirect("~/Employees/Details/" + appraisalDevelopmentPlan.AppraisalId);
        }
    }
}

