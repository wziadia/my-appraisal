﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Net;
using Employee_Appraisal.Models;

namespace Employee_Appraisal.Controllers
{
    public class AppraisalEmployeeController : Controller
    {
        private YIACOAPPSDBEntities db = new YIACOAPPSDBEntities();

        public ActionResult Index(string FinancialGoal)
        {
            return View();
        }

        // GET: AppraisalEmployee/Create
        public ActionResult Create(string appraisalId, string emplId, int jobFamily, string questionCode)
        {
            ViewBag.qCode = db.AppraisalQuestions.Where(ac => ac.QuestionCode == questionCode)
                .Select(l => l.QuestionText).FirstOrDefault();

            ViewBag.COQuestion = db.AppraisalQuestions.Where(a => a.QuestionGroup == "COMP").ToList();

            ViewBag.Year = new SelectList(db.AppraisalStatusYears
                .Where(st => st.Status == 1).ToList()
                , "Year", "Year");
            ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies
                .ToList()
                , "JobFamilyID", "JobFamilyName");
            ViewBag.COQuestions = new SelectList(db.AppraisalQuestions
                .Where(st => st.QuestionGroup == "COMP").ToList()
                , "QuestionCode", "Description");
            ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                .Where(st => st.QuestionGroup == "COMA").ToList()
                , "QuestionCode", "Description");
            if (jobFamily == 2)
            {
                ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                .Where(st => st.QuestionGroup == "COME").ToList()
                , "QuestionCode", "Description");
            }
            if (jobFamily == 3)
            {
                ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                .Where(st => st.QuestionGroup == "COML").ToList()
                , "QuestionCode", "Description");
            }
            if (jobFamily == 4)
            {
                ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                .Where(st => st.QuestionGroup == "COMS").ToList()
                , "QuestionCode", "Description");
            }
            ViewBag.AppraisalId = appraisalId;
            ViewBag.EmplId = emplId;
            return View();
        }

        //POST: AppraisalEmployee/Create
        //To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AppraisalId,EmplId,QuestionId,QuestionText,QuestionWeight,Q1,Q2,Q3,Q4,Actual,Q1A,Q2A,Q3A,Q4A,Score,Q1S,Q2S,Q3S,Q4S,QuestionFeedback,Expectation,Excellence,QuestionCode")] AppraisalEmployee appraisalEmployee)
        {
            var existingQuestionTotal = db.AppraisalEmployees.Where(l => l.EmplId == appraisalEmployee.EmplId
                        && l.QuestionCode == appraisalEmployee.QuestionCode
                        && l.AppraisalId == appraisalEmployee.AppraisalId)
                        .Select(l => l.QuestionId)
                        .Count();

            var employeeAppraisalJobFamily = db.Appraisals.Where(l => l.AppraisalId == appraisalEmployee.AppraisalId)
                .Select(l => l.JobFamily)
                .First();

            var existingQuestion = db.AppraisalEmployees.Where(l => l.EmplId == appraisalEmployee.EmplId
                        && l.QuestionCode != "ST1"
                        && l.QuestionCode != "R01"
                        && l.QuestionCode == appraisalEmployee.QuestionCode
                        && l.AppraisalId == appraisalEmployee.AppraisalId)
                        .Select(l => l.QuestionId)
                        .Count();

            var totalScore = db.AppraisalEmployees.Where(l => l.EmplId == appraisalEmployee.EmplId
                        && l.QuestionCode.Substring(0, 2) == appraisalEmployee.QuestionCode.Substring(0, 2)
                        && l.AppraisalId == appraisalEmployee.AppraisalId)
                        .Select(l => l.Score)
                        .DefaultIfEmpty(0)
                        .Sum();

            var totalWeight = db.AppraisalEmployees.Where(l => l.EmplId == appraisalEmployee.EmplId
                        && l.QuestionCode.Substring(0, 2) == appraisalEmployee.QuestionCode.Substring(0, 2)
                        && l.AppraisalId == appraisalEmployee.AppraisalId)
                        .Select(l => l.QuestionWeight)
                        .DefaultIfEmpty(0)
                        .Sum();

            var count = db.AppraisalEmployees.Where(l => l.EmplId == appraisalEmployee.EmplId
                        && l.QuestionCode.Substring(0, 2) == appraisalEmployee.QuestionCode.Substring(0, 2)
                        && l.AppraisalId == appraisalEmployee.AppraisalId)
                        .Select(l => l.QuestionId)
                        .Count();

            if (ModelState.IsValid)
            {
                if (existingQuestionTotal == 5)
                {
                    ViewBag.Year = new SelectList(db.AppraisalStatusYears
                        .Where(st => st.Status == 1).ToList()
                        , "Year", "Year");

                    ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies
                        .ToList()
                        , "JobFamilyID", "JobFamilyName");

                    ViewBag.COQuestions = new SelectList(db.AppraisalQuestions
                        .Where(st => st.QuestionGroup == "COMP").ToList()
                        , "QuestionCode", "Description");

                    ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                        .Where(st => st.QuestionGroup == "COMA").ToList()
                        , "QuestionCode", "Description");

                    if (employeeAppraisalJobFamily == 2)
                    {
                        ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COME").ToList()
                            , "QuestionCode", "Description");
                    }
                    if (employeeAppraisalJobFamily == 3)
                    {
                        ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COML").ToList()
                            , "QuestionCode", "Description");
                    }
                    if (employeeAppraisalJobFamily == 4)
                    {
                        ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COMS").ToList()
                            , "QuestionCode", "Description");
                    }

                    ViewBag.AppraisalId = appraisalEmployee.AppraisalId;
                    ViewBag.EmplId = appraisalEmployee.EmplId;
                    ViewBag.Error = "Should not exceed more than 5";

                    return View();
                }
                if (existingQuestion > 0)
                {
                    ViewBag.Year = new SelectList(db.AppraisalStatusYears
                        .Where(st => st.Status == 1).ToList()
                        , "Year", "Year");

                    ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies
                        .ToList()
                        , "JobFamilyID", "JobFamilyName");

                    ViewBag.COQuestions = new SelectList(db.AppraisalQuestions
                        .Where(st => st.QuestionGroup == "COMP").ToList()
                        , "QuestionCode", "Description");

                    ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                        .Where(st => st.QuestionGroup == "COMA").ToList()
                        , "QuestionCode", "Description");

                    if (employeeAppraisalJobFamily == 2)
                    {
                        ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COME").ToList()
                            , "QuestionCode", "Description");
                    }
                    if (employeeAppraisalJobFamily == 3)
                    {
                        ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COML").ToList()
                            , "QuestionCode", "Description");
                    }
                    if (employeeAppraisalJobFamily == 4)
                    {
                        ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COMS").ToList()
                            , "QuestionCode", "Description");
                    }

                    ViewBag.AppraisalId = appraisalEmployee.AppraisalId;
                    ViewBag.EmplId = appraisalEmployee.EmplId;
                    ViewBag.Error = "This objective is already exist.";

                    return View();
                }

                if (count + 1 == 5 && totalWeight + appraisalEmployee.QuestionWeight < 100)
                {
                    ViewBag.Year = new SelectList(db.AppraisalStatusYears
                        .Where(st => st.Status == 1).ToList()
                        , "Year", "Year");
                    ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies
                        .ToList()
                        , "JobFamilyID", "JobFamilyName");
                    ViewBag.COQuestions = new SelectList(db.AppraisalQuestions
                        .Where(st => st.QuestionGroup == "COMP").ToList()
                        , "QuestionCode", "Description");
                    ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                        .Where(st => st.QuestionGroup == "COMA").ToList()
                        , "QuestionCode", "Description");
                    if (employeeAppraisalJobFamily == 2)
                    {
                        ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COME").ToList()
                            , "QuestionCode", "Description");
                    }
                    if (employeeAppraisalJobFamily == 3)
                    {
                        ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COML").ToList()
                            , "QuestionCode", "Description");
                    }
                    if (employeeAppraisalJobFamily == 4)
                    {
                        ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COMS").ToList()
                            , "QuestionCode", "Description");
                    }

                    ViewBag.AppraisalId = appraisalEmployee.AppraisalId;
                    ViewBag.EmplId = appraisalEmployee.EmplId;
                    ViewBag.Error = "Questions total weight should be 100";

                    return View();
                }
                if (appraisalEmployee.Score <= (appraisalEmployee.QuestionWeight + (appraisalEmployee.QuestionWeight * 20 / 100)))
                {
                    if ((totalScore + appraisalEmployee.Score) <= 120 && (totalWeight + appraisalEmployee.QuestionWeight) <= 100)
                    {
                        db.AppraisalEmployees.Add(appraisalEmployee);

                        AppraisalLog appraisalLog = new AppraisalLog();

                        appraisalLog.AppraisalId = appraisalEmployee.AppraisalId;
                        appraisalLog.Appraisal = appraisalEmployee.Appraisal;
                        appraisalLog.LogDescription = "Objective Created.";
                        appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                        appraisalLog.LogAt = DateTime.Now;

                        db.AppraisalLogs.Add(appraisalLog);

                        db.SaveChanges();
                        ViewBag.Year = new SelectList(db.AppraisalStatusYears
                            .Where(st => st.Status == 1).ToList()
                            , "Year", "Year");
                        ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies
                            .ToList()
                            , "JobFamilyID", "JobFamilyName");
                        ViewBag.COQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COMP").ToList()
                            , "QuestionCode", "Description");
                        ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COMA").ToList()
                            , "QuestionCode", "Description");
                        if (employeeAppraisalJobFamily == 2)
                        {
                            ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COME").ToList()
                            , "QuestionCode", "Description");
                        }
                        if (employeeAppraisalJobFamily == 3)
                        {
                            ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COML").ToList()
                            , "QuestionCode", "Description");
                        }
                        if (employeeAppraisalJobFamily == 4)
                        {
                            ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                            .Where(st => st.QuestionGroup == "COMS").ToList()
                            , "QuestionCode", "Description");
                        }

                        ViewBag.AppraisalId = appraisalEmployee.AppraisalId;
                        ViewBag.EmplId = appraisalEmployee.EmplId;
                        ModelState.Clear();
                        ViewBag.Error = "Saved.";

                        return View(appraisalEmployee);
                    }
                }
            }

            ViewBag.Year = new SelectList(db.AppraisalStatusYears
                .Where(st => st.Status == 1).ToList()
                , "Year", "Year");
            ViewBag.COQuestions = new SelectList(db.AppraisalQuestions
                .Where(st => st.QuestionGroup == "COMP").ToList()
                , "QuestionCode", "Description");
            ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                .Where(st => st.QuestionGroup == "COMA").ToList()
                , "QuestionCode", "Description");
            if (employeeAppraisalJobFamily == 2)
            {
                ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                .Where(st => st.QuestionGroup == "COME").ToList()
                , "QuestionCode", "Description");
            }
            if (employeeAppraisalJobFamily == 3)
            {
                ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                .Where(st => st.QuestionGroup == "COML").ToList()
                , "QuestionCode", "Description");
            }
            if (employeeAppraisalJobFamily == 4)
            {
                ViewBag.JFQuestions = new SelectList(db.AppraisalQuestions
                .Where(st => st.QuestionGroup == "COMS").ToList()
                , "QuestionCode", "Description");
            }

            ViewBag.AppraisalId = appraisalEmployee.AppraisalId;
            ViewBag.EmplId = appraisalEmployee.EmplId;
            ViewBag.Error = "Questions total weight should Not exceed 100 points";

            return View();
        }

        // GET: AppraisalEmployee/SaveAppraisal
        public ActionResult SaveAppraisal(string ColumnToUpdate, String txtValue, int AppraisalId, int QuestionId, string EmplId)
        {
            //CompetenciesToUpdate coreCompetencies)
            int scale = 100;
            int count = 0;

            AppraisalEmployee upd = db.AppraisalEmployees.Where(ape => ape.EmplId == EmplId && ape.AppraisalId == AppraisalId && ape.QuestionId == QuestionId).FirstOrDefault();
            
            if (upd != null)
            {
                AppraisalLog appraisalLog = new AppraisalLog();
                Appraisal app = db.Appraisals.Where(ap => ap.AppraisalId == upd.AppraisalId).FirstOrDefault();

                switch (ColumnToUpdate)
                {
                    case "QuestionText":
                        upd.QuestionText = txtValue;

                        appraisalLog.AppraisalId = upd.AppraisalId;
                        appraisalLog.Appraisal = upd.Appraisal;
                        appraisalLog.LogDescription = "Objective Eited.";
                        appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                        appraisalLog.LogAt = DateTime.Now;

                        db.AppraisalLogs.Add(appraisalLog);

                        break;
                    case "QuestionWeight":
                        upd.QuestionWeight = Convert.ToInt32(txtValue);

                        appraisalLog.AppraisalId = upd.AppraisalId;
                        appraisalLog.Appraisal = upd.Appraisal;
                        appraisalLog.LogDescription = "Objective's Weight Eited.";
                        appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                        appraisalLog.LogAt = DateTime.Now;

                        db.AppraisalLogs.Add(appraisalLog);

                        if (upd.QuestionCode.ToString().Substring(0, 2) != "ST")
                            upd.Score = ((upd.Actual * scale) / (float)upd.QuestionWeight);
                        else
                        {
                            if (upd.Q1 != 0)
                            {
                                upd.Q1S = ((float)upd.Q1A / (float)upd.Q1) * 100;
                                count = count + 1;
                            }
                            else
                                upd.Q1S = 0;

                            if (upd.Q2 != 0)
                            {
                                upd.Q2S = ((float)upd.Q2A / (float)upd.Q2) * 100;
                                count = count + 1;
                            }
                            else
                                upd.Q2S = 0;

                            if (upd.Q3 != 0)
                            {
                                upd.Q3S = ((float)upd.Q3A / (float)upd.Q3) * 100;
                                count = count + 1;
                            }
                            else
                                upd.Q3S = 0;

                            if (upd.Q4 != 0)
                            {
                                upd.Q4S = ((float)upd.Q4A / (float)upd.Q4) * 100;
                                count = count + 1;
                            }
                            else
                                upd.Q4S = 0;

                            upd.Score = ((float)upd.Q1S + (float)upd.Q2S + (float)upd.Q3S + (float)upd.Q4S) / count;
                        }

                        break;
                    case "Q1":
                        upd.Q1 = Convert.ToInt32(txtValue);

                        if (upd.Q1 != 0)
                            upd.Q1S = ((float)upd.Q1A / (float)upd.Q1) * 100;
                        else
                            upd.Q1S = 0;

                        if (upd.Q1 != 0)
                            count = count + 1;
                        if (upd.Q2 != 0)
                            count = count + 1;
                        if (upd.Q3 != 0)
                            count = count + 1;
                        if (upd.Q4 != 0)
                            count = count + 1;

                        upd.Score = ((float)upd.Q1S + (float)upd.Q2S + (float)upd.Q3S + (float)upd.Q4S) / count;

                        appraisalLog.AppraisalId = upd.AppraisalId;
                        appraisalLog.Appraisal = upd.Appraisal;
                        appraisalLog.LogDescription = "Quarter 1 Target Eited.";
                        appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                        appraisalLog.LogAt = DateTime.Now;

                        db.AppraisalLogs.Add(appraisalLog);

                        break;
                    case "Q2":
                        upd.Q2 = Convert.ToInt32(txtValue);

                        appraisalLog.AppraisalId = upd.AppraisalId;
                        appraisalLog.Appraisal = upd.Appraisal;
                        appraisalLog.LogDescription = "Quarter 2 Target Eited.";
                        appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                        appraisalLog.LogAt = DateTime.Now;

                        db.AppraisalLogs.Add(appraisalLog);

                        if (upd.Q2 != 0)
                            upd.Q2S = ((float)upd.Q2A / (float)upd.Q2) * 100;
                        else
                            upd.Q2S = 0;

                        if (upd.Q1 != 0)
                            count = count + 1;
                        if (upd.Q2 != 0)
                            count = count + 1;
                        if (upd.Q3 != 0)
                            count = count + 1;
                        if (upd.Q4 != 0)
                            count = count + 1;

                        upd.Score = ((float)upd.Q1S + (float)upd.Q2S + (float)upd.Q3S + (float)upd.Q4S) / count;

                        break;
                    case "Q3":
                        upd.Q3 = Convert.ToInt32(txtValue);

                        appraisalLog.AppraisalId = upd.AppraisalId;
                        appraisalLog.Appraisal = upd.Appraisal;
                        appraisalLog.LogDescription = "Quarter 3 Target Eited.";
                        appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                        appraisalLog.LogAt = DateTime.Now;

                        db.AppraisalLogs.Add(appraisalLog);

                        if (upd.Q3 != 0)
                            upd.Q3S = ((float)upd.Q3A / (float)upd.Q3) * 100;
                        else
                            upd.Q3S = 0;

                        if (upd.Q1 != 0)
                            count = count + 1;
                        if (upd.Q2 != 0)
                            count = count + 1;
                        if (upd.Q3 != 0)
                            count = count + 1;
                        if (upd.Q4 != 0)
                            count = count + 1;

                        upd.Score = ((float)upd.Q1S + (float)upd.Q2S + (float)upd.Q3S + (float)upd.Q4S) / count;

                        break;
                    case "Q4":
                        upd.Q4 = Convert.ToInt32(txtValue);

                        appraisalLog.AppraisalId = upd.AppraisalId;
                        appraisalLog.Appraisal = upd.Appraisal;
                        appraisalLog.LogDescription = "Quarter 4 Target Eited.";
                        appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                        appraisalLog.LogAt = DateTime.Now;

                        db.AppraisalLogs.Add(appraisalLog);

                        if (upd.Q4 != 0)
                            upd.Q4S = ((float)upd.Q4A / (float)upd.Q4) * 100;
                        else
                            upd.Q4S = 0;

                        if (upd.Q1 != 0)
                            count = count + 1;
                        if (upd.Q2 != 0)
                            count = count + 1;
                        if (upd.Q3 != 0)
                            count = count + 1;
                        if (upd.Q4 != 0)
                            count = count + 1;

                        upd.Score = ((float)upd.Q1S + (float)upd.Q2S + (float)upd.Q3S + (float)upd.Q4S) / count;

                        break;
                    case "Actual":
                        upd.Actual = (float)Convert.ToInt32(txtValue);

                        upd.Score = ((float)upd.Actual * scale) / upd.QuestionWeight;

                        if (upd.Score > 0)
                        {
                            app.Stage = 4;

                            appraisalLog.AppraisalId = upd.AppraisalId;
                            appraisalLog.Appraisal = upd.Appraisal;
                            appraisalLog.LogDescription = "Appraisal Evaluated.";
                            appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                            appraisalLog.LogAt = DateTime.Now;

                            db.AppraisalLogs.Add(appraisalLog);

                        }

                        break;
                    case "Q1A":
                        upd.Q1A = (float)Convert.ToInt32(txtValue);

                        if (upd.Q1 != 0 && (((upd.Q1A * 100) / upd.Q1) <= 120))
                            upd.Q1S = ((float)upd.Q1A / (float)upd.Q1) * 100;
                        else
                        { upd.Q1S = 0; upd.Q1A = 0; }

                        if (upd.Q1 != 0)
                            count = count + 1;
                        if (upd.Q2 != 0)
                            count = count + 1;
                        if (upd.Q3 != 0)
                            count = count + 1;
                        if (upd.Q4 != 0)
                            count = count + 1;

                        upd.Score = ((float)upd.Q1S + (float)upd.Q2S + (float)upd.Q3S + (float)upd.Q4S) / count;

                        break;
                    case "Q2A":
                        upd.Q2A = (float)Convert.ToInt32(txtValue);

                        if (upd.Q2 != 0 && (((upd.Q2A * 100) / upd.Q2) <= 120))
                            upd.Q2S = ((float)upd.Q2A / (float)upd.Q2) * 100;
                        else
                        { upd.Q1S = 0; upd.Q2A = 0; }

                        if (upd.Q1 != 0)
                            count = count + 1;
                        if (upd.Q2 != 0)
                            count = count + 1;
                        if (upd.Q3 != 0)
                            count = count + 1;
                        if (upd.Q4 != 0)
                            count = count + 1;

                        upd.Score = ((float)upd.Q1S + (float)upd.Q2S + (float)upd.Q3S + (float)upd.Q4S) / count;

                        break;
                    case "Q3A":
                        upd.Q3A = (float)Convert.ToInt32(txtValue);

                        if (upd.Q3 != 0 && (((upd.Q3A * 100) / upd.Q3) <= 120))
                            upd.Q3S = ((float)upd.Q3A / (float)upd.Q3) * 100;
                        else
                        { upd.Q3S = 0; upd.Q3A = 0; }

                        if (upd.Q1 != 0)
                            count = count + 1;
                        if (upd.Q2 != 0)
                            count = count + 1;
                        if (upd.Q3 != 0)
                            count = count + 1;
                        if (upd.Q4 != 0)
                            count = count + 1;

                        upd.Score = ((float)upd.Q1S + (float)upd.Q2S + (float)upd.Q3S + (float)upd.Q4S) / count;

                        break;
                    case "Q4A":
                        upd.Q4A = (float)Convert.ToInt32(txtValue);

                        if (upd.Q4 != 0 && (((upd.Q4A * 100) / upd.Q4) <= 120))
                            upd.Q4S = ((float)upd.Q4A / (float)upd.Q4) * 100;
                        else
                        { upd.Q4S = 0; upd.Q4A = 0; }

                        if (upd.Q1 != 0)
                            count = count + 1;
                        if (upd.Q2 != 0)
                            count = count + 1;
                        if (upd.Q3 != 0)
                            count = count + 1;
                        if (upd.Q4 != 0)
                            count = count + 1;

                        upd.Score = ((float)upd.Q1S + (float)upd.Q2S + (float)upd.Q3S + (float)upd.Q4S) / count;

                        break;
                    default:
                        break;
                }
                if (ModelState.IsValid)
                {
                    db.Entry(upd).State = EntityState.Modified;

                    db.SaveChanges();
                }
            }
            ViewBag.Year = new SelectList(db.AppraisalStatusYears, "Year", "Year");
            ViewBag.JobFamily = new SelectList(db.AppraisalJobFamilies, "JobFamilyID", "JobFamilyName");
            return RedirectToAction("Details/" + AppraisalId.ToString(), "Employees", null);
        }

        // GET: Appraisals/Delete?appraisalId=989&emplId=001784&questionId=R04
        public ActionResult Delete(int? appraisalId, string emplId, int questionId)
        {
            if (appraisalId == null || emplId == null || questionId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppraisalEmployee appraisalEmployee = db.AppraisalEmployees.Find(emplId, appraisalId, questionId);
            if (appraisalEmployee == null)
            {
                return HttpNotFound();
            }

            return View(appraisalEmployee);
        }

        // GET: Appraisals/Delete?appraisalId=989&emplId=001784&questionId=R04
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? appraisalId, string emplId, int questionId)
        {
            if (appraisalId == null || emplId == null || questionId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppraisalEmployee appraisalEmployee = db.AppraisalEmployees.Find(emplId, appraisalId, questionId);
            if (appraisalEmployee == null)
            {
                return HttpNotFound();
            }

            db.AppraisalEmployees.Remove(appraisalEmployee);

            AppraisalLog appraisalLog = new AppraisalLog();

            appraisalLog.AppraisalId = appraisalEmployee.AppraisalId;
            appraisalLog.Appraisal = appraisalEmployee.Appraisal;
            appraisalLog.LogDescription = "Objective Deleted.";
            appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
            appraisalLog.LogAt = DateTime.Now;

            db.AppraisalLogs.Add(appraisalLog);

            db.SaveChanges();

            return Redirect("~/Employees/Details/" + appraisalEmployee.AppraisalId);
        }
    }
}