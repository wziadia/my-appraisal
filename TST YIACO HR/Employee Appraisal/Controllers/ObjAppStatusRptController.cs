﻿using Employee_Appraisal.AIFEmployee;
using Employee_Appraisal.AIFRepRel;
using Employee_Appraisal.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Employee_Appraisal.Controllers
{
    public class ObjAppStatusRptController : Controller
    {
        private YIACOAPPSDBEntities db = new YIACOAPPSDBEntities();
        List<ObjReport> Objquery = new List<ObjReport>();
        List<AppReport> Appquery = new List<AppReport>();
        List<AppReport> RedeemAppData = new List<AppReport>();
        List<AppTotalReport> AppTotalquery = new List<AppTotalReport>();

        public class ObjReport
        {
            public string managerId { get; set; }
            public string managerName { get; set; }
            public int StaffNo { get; set; }
            public int Created { get; set; }
            public int NotCreated { get; set; }
            public int? Completed { get; set; }
            public int? NotCompleted { get; set; }
            public int? Accepted { get; set; }
            public int? Rejected { get; set; }
            public int? NotViewed { get; set; }
        }

        public class AppTotalReport
        {
            public string managerId { get; set; }
            public string managerName { get; set; }
            public int StaffNo { get; set; }
            public int Created { get; set; }
            public int NotCreated { get; set; }
            public int? Completed { get; set; }
            public int? NotCompleted { get; set; }
            public int? Accepted { get; set; }
            public int? Rejected { get; set; }
            public int? NotViewed { get; set; }
        }

        public class AppReport
        {
            public string managerId { get; set; }
            public string managerName { get; set; }
            public string empId { get; set; }
            public string empName { get; set; }
            public string civilId { get; set; }
            public DateTime joinDate { get; set; }
            public string org { get; set; }
            public string position { get; set; }
            public string IsCreated { get; set; }
            public string IsCompleted { get; set; }
            public string IsAccepted { get; set; }
        }

        public List<AxdEntity_SysCompanyRepRel_1> getReportingRelationships()
        {
            EmplRepRelServiceClient client = new EmplRepRelServiceClient();

            OperationContextScope o = new OperationContextScope(client.InnerChannel);

            AIFRepRel.QueryCriteria qc = new AIFRepRel.QueryCriteria();

            AxdEmplRepRel emplRepRel = new AxdEmplRepRel();

            qc.CriteriaElement = new AIFRepRel.CriteriaElement[1];
            qc.CriteriaElement[0] = new AIFRepRel.CriteriaElement();
            qc.CriteriaElement[0].DataSourceName = "SysCompanyRepRel_1";
            qc.CriteriaElement[0].Operator = AIFRepRel.Operator.NotEqual;
            qc.CriteriaElement[0].FieldName = "ReportsTo";
            qc.CriteriaElement[0].Value1 = "000000";

            try
            {
                emplRepRel = client.find(qc);

                if (emplRepRel != null)
                {
                    return emplRepRel.SysCompanyRepRel_1.ToList();
                }
                return null;
            }
            catch (Exception aifException)
            {
                return null;
            }
            finally
            {
                client.Close();
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ObjStatus(string ORG, int YR = 0)
        {
            ViewBag.ORGANIZATION = new SelectList(AxdEntity_EmplTable_1.ConstructEmployeeList().Select(st => st.OrganizationUnitName == null ? "None" : st.OrganizationUnitName).Distinct().OrderBy(OrganizationUnitName => OrganizationUnitName).ToList());
            ViewBag.YEAR = new SelectList(db.AppraisalStatusYears.Select(st => st.Year).Distinct().OrderBy(Year => Year).ToList());

            List<ObjReport> reportResult = new List<ObjReport>();
            List<AxdEntity_SysCompanyRepRel_1> Employees = getReportingRelationships().ToList();

            if (ORG != null)
            {
                if (ORG != "Select all" && ORG != "")
                    Employees = getReportingRelationships().Where(employee => employee.Email == ORG).ToList();
            }
            using (YIACOAPPSDBEntities dbContext = new YIACOAPPSDBEntities())
            {
                List<Appraisal> Appraisals = dbContext.Appraisals.Where(year => year.Year == YR).ToList();
                List<string> Managers = Employees.Select(employee => employee.ReportsTo).Distinct().ToList();

                for (int i = 0; i < Managers.Count(); i++)
                {
                    if (Managers[i] != null)
                    {
                        reportResult.Add(new ObjReport()
                        {
                            managerId = Managers[i].ToString(),
                            managerName = (AxdEntity_EmplTable_1.ConstructEmployeeById(Managers[i].ToString(), "YIAC") == null ? AxdEntity_EmplTable_1.ConstructEmployeeById(Managers[i].ToString(), "PAY") == null ? "" : AxdEntity_EmplTable_1.ConstructEmployeeById(Managers[i].ToString(), "PAY").Name : AxdEntity_EmplTable_1.ConstructEmployeeById(Managers[i].ToString(), "YIAC").Name),
                            StaffNo = Employees.Count(empl => empl.ReportsTo == Managers[i]),
                            Created = Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage >= 0).Count(),
                            NotCreated = (Employees.Count(empl => empl.ReportsTo == Managers[i])) - (Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage >= 0).Count()),
                            Completed = Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage >= 1).Count(),
                            NotCompleted = (Appraisals.Where(app => app.AppraiserId == Managers[i]).Count()) - (Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage >= 1).Count()),
                            Accepted = Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage == 2).Count(),
                            Rejected = Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage == 3).Count(),
                            //NotViewed = ((row.Select(employeeStaff => employeeStaff.EmplId).Count()) - row.Where(o => o.AcceptObjectives == 1).Sum(accp => accp.AcceptObjectives) - row.Where(o => o.AcceptObjectives == 0).Sum(accp => accp.AcceptObjectives))
                        });
                    }
                }
                ViewData["ObjStat"] = reportResult;

                return View();
            }
        }

        public ActionResult AppTotalStatus(string ORG)
        {
            int YR = DateTime.Now.Year;
            //if (DateTime.Now.Month == 1)
            //    YR = DateTime.Now.Year - 1;
            ViewBag.ORGANIZATION = new SelectList(AxdEntity_EmplTable_1.ConstructEmployeeList().Select(st => st.OrganizationUnitName == null ? "None" : st.OrganizationUnitName).Distinct().OrderBy(OrganizationUnitName => OrganizationUnitName).ToList());
            ViewBag.YEAR = new SelectList(db.AppraisalStatusYears.Select(st => st.Year).Distinct().OrderBy(Year => Year).ToList());

            List<AppTotalReport> reportResult = new List<AppTotalReport>();
            List<AxdEntity_SysCompanyRepRel_1> Employees = getReportingRelationships().ToList();

            if (ORG != null)
            {
                if (ORG != "Select all" && ORG != "")
                    Employees = getReportingRelationships().Where(employee => employee.Email == ORG).ToList();
            }

            using (YIACOAPPSDBEntities dbContext = new YIACOAPPSDBEntities())
            {
                List<string> Managers = Employees.Select(employee => employee.ReportsTo).Distinct().ToList();
                List<Appraisal> Appraisals = dbContext.Appraisals.Where(year => year.Year == YR).ToList();

                for (int i = 0; i < Managers.Count(); i++)
                {
                    if (Managers[i] != null)
                    {
                        reportResult.Add(new AppTotalReport()
                            {
                                managerId = Managers[i].ToString(),
                                managerName = (AxdEntity_EmplTable_1.ConstructEmployeeById(Managers[i].ToString(), "YIAC") == null ? AxdEntity_EmplTable_1.ConstructEmployeeById(Managers[i].ToString(), "PAY") == null ? "" : AxdEntity_EmplTable_1.ConstructEmployeeById(Managers[i].ToString(), "PAY").Name : AxdEntity_EmplTable_1.ConstructEmployeeById(Managers[i].ToString(), "YIAC").Name),
                                StaffNo = Employees.Count(empl => empl.ReportsTo == Managers[i]),
                                Created = Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage >= 4).Count(),
                                NotCreated = (Employees.Count(empl => empl.ReportsTo == Managers[i])) - (Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage >= 4).Count()),
                                Completed = Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage >= 5).Count(),
                                NotCompleted = (Appraisals.Where(app => app.AppraiserId == Managers[i]).Count()) - (Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage >= 5).Count()),
                                Accepted = Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage == 6).Count(),
                                Rejected = Appraisals.Where(app => app.AppraiserId == Managers[i] && app.Stage == 7).Count(),
                                //NotViewed = (Appraisals.Where(app => app.AppraiserId == Managers[i]).Count()) - (Appraisals.Where(app => app.AppraiserId == Managers[i] && app.AcceptAppraisal == 1).Count() + Appraisals.Where(app => app.AppraiserId == Managers[i] && app.AcceptAppraisal == 0).Count())
                            });
                    }
                }

                ViewData["AppTotalStat"] = reportResult;

                return View();
            }
        }

        public ActionResult AppStatus(string ORG, int YR = 0)
        {
            ViewBag.ORGANIZATION = new SelectList(AxdEntity_EmplTable_1.ConstructEmployeeList().Select(st => st.OrganizationUnitName == null ? "None" : st.OrganizationUnitName).Distinct().OrderBy(OrganizationUnitName => OrganizationUnitName).ToList());
            ViewBag.YEAR = new SelectList(db.AppraisalStatusYears.Select(st => st.Year).Distinct().OrderBy(Year => Year).ToList());
            List<AppReport> reportResult = new List<AppReport>();
            List<AxdEntity_EmplTable_1> Employees = AxdEntity_EmplTable_1.ConstructEmployeeList().ToList();

            if (ORG != null)
            {
                if (ORG != "Select all" && ORG != "")
                    Employees = AxdEntity_EmplTable_1.ConstructEmployeeList().Where(st => st.OrganizationUnitName == ORG).ToList();
            }
            using (YIACOAPPSDBEntities dbContext = new YIACOAPPSDBEntities())
            {
                List<Appraisal> Appraisals = dbContext.Appraisals.Where(year => year.Year == YR).ToList();

                for (int i = 0; i < Employees.Count(); i++)
                {
                    reportResult.Add(new AppReport()
                        {
                            empId = (Employees[i].EmplId == null ? "" : Employees[i].EmplId.ToString()),
                            empName = (Employees[i].Name == null ? "" : Employees[i].Name.ToString()),
                            civilId = (Employees[i].CivilID == null ? "" : Employees[i].CivilID.ToString()),
                            joinDate = Employees[i].JoinDate,
                            org = (Employees[i].OrganizationUnitName == null ? "" : Employees[i].OrganizationUnitName.ToString()),
                            position = (Employees[i].TitleNameEN == null ? "" : Employees[i].TitleNameEN.ToString()),
                            managerId = Employees[i].ReportingToId.ToString(),
                            managerName = (AxdEntity_EmplTable_1.ConstructEmployeeById(Employees[i].ReportingToId.ToString(), "YIAC") == null ? AxdEntity_EmplTable_1.ConstructEmployeeById(Employees[i].ReportingToId.ToString(), "PAY") == null ? "" : AxdEntity_EmplTable_1.ConstructEmployeeById(Employees[i].ReportingToId.ToString(), "PAY").Name : AxdEntity_EmplTable_1.ConstructEmployeeById(Employees[i].ReportingToId.ToString(), "YIAC").Name),
                            IsCreated = (Appraisals != null ? Appraisals.Where(app => app.EmplId == Employees[i].EmplId).Select(st => st.Stage).FirstOrDefault() >= 4 ? "YES" : "NO" : "NO"),
                            IsCompleted = (Appraisals != null ? Appraisals.Where(app => app.EmplId == Employees[i].EmplId).Select(st => st.Stage).FirstOrDefault() >= 5 ? "YES" : "NO" : "NO"),
                            IsAccepted = (Appraisals != null ? Appraisals.Where(app => app.EmplId == Employees[i].EmplId).Select(st => st.Stage).FirstOrDefault() == 6 ? "YES" : "NO" : "NO")
                        });
                    }
                }
                ViewData["AppStat"] = reportResult;

                return View();
            }
    }
}