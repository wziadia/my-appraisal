﻿using Employee_Appraisal.AIFEmployee;
using Employee_Appraisal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using Employee_Appraisal.AIFReportingRelationships;
using Employee_Appraisal.AIFRepRel;
using System.ServiceModel.Channels;
using System.ServiceModel;
using System.Data;

namespace Employee_Appraisal.Controllers
{
    public class EmployeesController : Controller
    {
        private YIACOAPPSDBEntities db = new YIACOAPPSDBEntities();
        AxdEntity_EmplTable_1 employee;

        // GET: Employees
        public ActionResult Index(string login)
        {
            employee = AxdEntity_EmplTable_1.ConstructEmployee(login, "YIAC");
            if (employee == null)
                employee = AxdEntity_EmplTable_1.ConstructEmployee(login, "PAY");

            List<AxdEntity_EmplTable_1> axdEntity_EmplTable_1 = employee.getSubordinates();

            return View(axdEntity_EmplTable_1);
        }

        // GET: Employees/Appraisals
        public ActionResult Appraisals(string id, string name)
        {
            IQueryable<Appraisal> appraisal = null;

            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                appraisal = db.Appraisals.Where(emplID => emplID.EmplId == id && (emplID.Deleted == 0 || emplID.Deleted == null));

                ViewBag.EmplID = id;

                ViewData["EmplName"] = name;

            }
            catch (Exception e)
            {
                ViewData["ErrorMessage"] = e.StackTrace;
            }

            return View(appraisal);
        }

        // GET: Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appraisal appraisal = db.Appraisals.Find(id);
            if (appraisal == null)
            {
                return HttpNotFound();
            }

            ViewData["AppraisalId"] = appraisal.AppraisalId;
            ViewData["EmplId"] = appraisal.EmplId;
            ViewData["name"] = appraisal.Employee.Name;
            ViewBag.AcceptObjectives = appraisal.Stage;
            ViewBag.AcceptAppraisal = appraisal.Stage;

            return View(appraisal);
        }

        public ActionResult SendObjectivesAcceptance(string emplId, int appraisalId)
        {
            employee = AxdEntity_EmplTable_1.ConstructEmployeeById(emplId, "YIAC");
            if (employee.EmplId == null)
                employee = AxdEntity_EmplTable_1.ConstructEmployeeById(emplId, "PAY");

            Appraisal appraisal = db.Appraisals.Find(appraisalId);

            if (ModelState.IsValid)
            {
                if (employee.Email != null)
                {
                    string[] lines = { "FROM: itd@yiaco.com",
                                 "TO: " + employee.Email,
                                 "SUBJECT: Employee Performance Objectives for the year "+appraisal.Year+"",
                                 "",
                                 "Dear "+ employee.Name +",",
                                 "",
                                 "Please click the below link to review your objectives for the year "+appraisal.Year+".",
                                 "",
                                 "http://yiacoinet01:8016/Appraisal",
                                 "",
                                 "After logging in, click on 'My Appraisal', then click on 'Details' beside this year's appraisal '"+appraisal.Year+"', Scroll down to the bottom and click on either Accept Objectives or Reject Objectives and enter your Feedback in the box provided.",
                                 "",
                                 "Thanks & Best Regards,",
                                 employee.Manager.Name
                             };
                    System.IO.File.WriteAllLines(@"C:\inetpub\mailroot\Pickup\WriteLines.txt", lines);
                    //System.IO.File.WriteAllLines(@"C:\CopyTest\WriteLines.txt", lines);

                    appraisal.Stage = 1;

                    db.Entry(appraisal).State = EntityState.Modified;

                    AppraisalLog appraisalLog = new AppraisalLog();

                    appraisalLog.AppraisalId = appraisal.AppraisalId;
                    appraisalLog.Appraisal = appraisal;
                    appraisalLog.LogDescription = "Objectives Sent.";
                    appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                    appraisalLog.LogAt = DateTime.Now;

                    db.AppraisalLogs.Add(appraisalLog);

                    db.SaveChanges();
                }
                else
                {
                    //NOT SENT
                }
            }

            return RedirectToAction("Details/" + appraisal.AppraisalId.ToString(), "Employees", null);
        }

        public ActionResult SendAppraisalAcceptance(string emplId, int appraisalId, string appraisalRemarks)
        {
            employee = AxdEntity_EmplTable_1.ConstructEmployeeById(emplId, "YIAC");
            if (employee.EmplId == null)
                employee = AxdEntity_EmplTable_1.ConstructEmployeeById(emplId, "PAY");

            Appraisal appraisal = db.Appraisals.Find(appraisalId);

            if (ModelState.IsValid)
            {
                if (employee.Email != null)
                {
                    string[] lines = { "FROM: itd@yiaco.com",
                                 "TO: " + employee.Email,// employee.SysCompanyADInfo_1[0].NetworkAlias + "@yiaco.com",
                                 "SUBJECT: Employee Performance Appraisal for the year " + appraisal.Year,
                                 "",
                                 "Dear "+ employee.Name +",",
                                 "",
                                 "Please click the below link to review your appraisal for the year " + appraisal.Year + ".",
                                 "",
                                 "http://yiacoinet01:8016/Appraisal",
                                 "",
                                 "After logging in, click on 'My Appraisal', then click on 'Details' beside this year's appraisal '" + appraisal.Year + "', Scroll down to the bottom and click on either Accept Appraisal or Reject Appraisal and enter your Feedback in the box provided.",
                                 "",
                                 "Thanks & Best Regards,",
                                 employee.Manager.Name
                             };
                    System.IO.File.WriteAllLines(@"C:\inetpub\mailroot\Pickup\WriteLines.txt", lines);
                    //System.IO.File.WriteAllLines(@"C:\CopyTest\WriteLines.txt", lines);

                    appraisal.Stage = 5;
                    appraisal.AppraisalRemarks = appraisalRemarks;

                    db.Entry(appraisal).State = EntityState.Modified;

                    AppraisalLog appraisalLog = new AppraisalLog();

                    appraisalLog.AppraisalId = appraisal.AppraisalId;
                    appraisalLog.Appraisal = appraisal;
                    appraisalLog.LogDescription = "Appraisal Sent.";
                    appraisalLog.LogBy = this.User.Identity.Name.Remove(0, this.User.Identity.Name.LastIndexOf('\\') + 1);
                    appraisalLog.LogAt = DateTime.Now;

                    db.AppraisalLogs.Add(appraisalLog);

                    db.SaveChanges();

                    return RedirectToAction("Details/" + appraisal.AppraisalId.ToString(), "Employees", null);
                }
                else
                {
                    //NOT SET

                    return RedirectToAction("Details/" + appraisal.AppraisalId.ToString(), "Employees", null);

                }
            }

            return View();
        }

        public ActionResult SaveComment(int appraisalId, string appraisalRemarks)
        {
            Appraisal appraisal = db.Appraisals.Find(appraisalId);

            if (ModelState.IsValid)
            {
                appraisal.AppraisalRemarks = appraisalRemarks;
                db.Entry(appraisal).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
            return RedirectToAction("Details/" + appraisal.AppraisalId.ToString(), "Employees", null);
        }

    }
}