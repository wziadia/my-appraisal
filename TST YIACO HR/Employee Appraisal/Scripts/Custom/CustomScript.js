﻿
$(document).ready(function () {

    ////// Sales Objectives Scripts
    if (getParameterByName("question") == "SLText")
        window.location.hash = '#Sl';

    ////// Operational Objectives Scripts
    if (getParameterByName("question") == "OPText")
        window.location.hash = '#Op';

    ////// Development Plan Scripts
    if (getParameterByName("question") == "Dev")
        window.location.hash = '#Dev';

    ////// Functional Competencies Scripts
    if (getParameterByName("question") == "Fun")
        window.location.hash = '#Fun';

    ///// Edit Scripts
    if (document.getElementById('STAG') != null) {
        if (document.getElementById('STAG').value <= 1 || document.getElementById('STAG').value == 3) {
            ////// Sales Objectives Scripts
            $(".SLQuestionText").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "QuestionText");
            });
            $(".SLQuestionWeight").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "QuestionWeight");
            });
            $(".SLQuestionWeight1").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Q1");
            });
            $(".SLQuestionWeight2").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Q2");
            });
            $(".SLQuestionWeight3").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Q3");
            });
            $(".SLQuestionWeight4").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Q4");
            });

            ////// Operational Objectives Scripts
            $(".OPQuestionText").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "QuestionText");
            });
            $(".OPQuestionWeight").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "QuestionWeight");
            });

            ////// Core Objectives Scripts
            $(".COQuestionWeight").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "QuestionWeight");
            });

            ////// Core Functional Scripts
            $(".FNQuestionWeight").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "QuestionWeight");
            });
        }

        if ((document.getElementById('STAG').value >= 3 && document.getElementById('STAG').value < 5) || document.getElementById('STAG').value == 2) {
            ////// Sales Appraisal Scripts
            $(".SLActual1").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Q1A");
            });
            $(".SLActual2").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Q2A");
            });
            $(".SLActual3").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Q3A");
            });
            $(".SLActual4").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Q4A");
            });

            ////// Operational Appraisal Scripts
            $(".OPActual").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Actual");
            });

            ////// Core Appraisal Scripts
            $(".COActual").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Actual");
            });

            ////// Functionsl Appraisal Scripts
            $(".FNActual").dblclick(function (e) {
                e.stopPropagation();
                var CurrentEle = $(this);
                var value = $(this).html();
                var row = $(e.target).closest('tr');
                var AppraisalId = row.find($("[id*=key]")).val();
                var QuestionId = row.find($("[id*=key2]")).val();
                UpdateVal(CurrentEle, value, AppraisalId, QuestionId, "Actual");
            });
        }
    }

    ///// Calculate total of scores
    window.onload = function () {
        var sum = 0;
        var count = 0;
        $('.SLScore').each(function () {
            sum += parseInt($(this).text());
            count = count + 1;
        });
        if (document.getElementById('SLTotalScore') !== null) {
            document.getElementById('SLTotalScore').value = (sum.toString() / count);
            document.getElementById('SLTotalScore').value = Math.round(document.getElementById('SLTotalScore').value);
        }

        sum = 0;
        count = 0;
        $('.OPScore').each(function () {
            sum += parseInt($(this).text());
            count = count + 1;
        });
        if (document.getElementById('OPTotalScore') !== null) {
            document.getElementById('OPTotalScore').value = (sum.toString() / count);
            document.getElementById('OPTotalScore').value = Math.round(document.getElementById('OPTotalScore').value);
        }

        sum = 0;
        count = 0;
        $('.COScore').each(function () {
            sum += parseInt($(this).text());
            count = count + 1;
        });
        if (document.getElementById('COTotalScore') !== null) {
            document.getElementById('COTotalScore').value = (sum.toString() / count);
            document.getElementById('COTotalScore').value = Math.round(document.getElementById('COTotalScore').value);
        }

        sum = 0;
        count = 0;
        $('.FNScore').each(function () {
            sum += parseInt($(this).text());
            count = count + 1;
        });
        if (document.getElementById('FNTotalScore') !== null) {
            document.getElementById('FNTotalScore').value = (sum.toString() / count);
            document.getElementById('FNTotalScore').value = Math.round(document.getElementById('FNTotalScore').value);
        }

        sum = 0;

        sum = (((Number($('#OPTotalScore').val()) * 6) + (Number($('#COTotalScore').val()) * 2) + (Number($('#FNTotalScore').val()) * 2)) / 10);

        if (typeof financialGoal != 'undefined') {
            if (financialGoal == 1)
                sum = ((Number(($('#SLTotalScore').val()) * 6) + Number(($('#OPTotalScore').val()) * 2) + Number($('#COTotalScore').val()) + Number($('#FNTotalScore').val())) / 10);
        }

        if (sum > 0 && sum <= 60)
            document.getElementById('TotalScore').value = parseFloat(sum).toFixed(2).toString() + " %    Below Expecation";
        if (sum > 60 && sum <= 80)
            document.getElementById('TotalScore').value = parseFloat(sum).toFixed(2).toString() + " %    Needs Improvement";
        if (sum > 80 && sum <= 109)
            document.getElementById('TotalScore').value = parseFloat(sum).toFixed(2).toString() + " %    Meets expectations";
        if (sum > 109 && sum <= 115)
            document.getElementById('TotalScore').value = parseFloat(sum).toFixed(2) + " %    Exceeds Expectation";
        if (sum > 115 && sum <= 120)
            document.getElementById('TotalScore').value = parseFloat(sum).toFixed(2).toString() + " %    Outstanding";

        if (($('#OPTotalScore').val() == undefined ||
            $('#COTotalScore').val() == undefined ||
            $('#FNTotalScore').val() == undefined) &&
            $('#TotalScore').val() != undefined)
            document.getElementById('TotalScore').value = "You have to complete all sections";
    }
});

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)')
                    .exec(window.location.search);
    return match ?
        decodeURIComponent(match[1].replace(/\+/g, ' '))
        : null;
}

function UpdateVal(CurrentEle, value, AppraisalId, QuestionId, ColumnToUpdate) {
    if ($(".cellValue") !== undefined) {
        if ($(".cellValue").val() !== undefined) {
            $(".cellValue").parent().html($("#OriginalValue").val().trim());
            $(".cellValue").remove();
        }
    }
    if (value.match("<") == null) {
        var wght = CurrentEle.attr('id');
        if (wght.substring(10, 13) == "Txt") {
            $(CurrentEle).html('<div class="cellValue" id="cellWrapper"> ' +
                '<textarea rows="2" width="1000px" cols="1000" class="cellValue" type="text" id="txtValue">' + value + '</textarea><br/>' +
                '<input class="cellValue" type="hidden" value="' + AppraisalId + '" id="keySelected" />' +
                '<input class="cellValue" type="hidden" value="' + QuestionId + '" id="keySelected2" />' +
                '<input class="cellValue" type="hidden" value="' + ColumnToUpdate + '" id="ColumnToUpdate" /> ' +
                '<input class="cellValue" type="hidden" value="' + value + '" id="OriginalValue" /> ' +
                '<input class="cellValue" type="button" value="save"   onclick="return FNSaveChanges()" /> ' +
                '<input class="cellValue" type="button" value="cancel" onclick="return CancelChanges()" /> ' +
                '</div> ');
        }
        else {
            $(CurrentEle).html('<div class="cellValue" id="cellWrapper"> ' +
                '<input class="cellValue" type="text" id="txtValue" value="' + value + '" /><br/>' +
                '<input class="cellValue" type="hidden" value="' + AppraisalId + '" id="keySelected" />' +
                '<input class="cellValue" type="hidden" value="' + QuestionId + '" id="keySelected2" />' +
                '<input class="cellValue" type="hidden" value="' + ColumnToUpdate + '" id="ColumnToUpdate" /> ' +
                '<input class="cellValue" type="hidden" value="' + value + '" id="OriginalValue" /> ' +
                '<input class="cellValue" type="button" value="save"   onclick="return FNSaveChanges()" /> ' +
                '<input class="cellValue" type="button" value="cancel" onclick="return CancelChanges()" /> ' +
                '</div> ');
        }
    }
    $(".cellValue").focus();
    $(".cellValue").keyup(function (event) {
        if (event.keyCode == 13) {
            $(CurrentEle).html($(".cellValue").val().trim());
        }
    });
}

function CancelChanges(e) {
    if ($(".cellValue") !== undefined) {
        if ($(".cellValue").val() !== undefined) {
            $(".cellValue").parent().html($("#OriginalValue").val().trim());
            $(".cellValue").remove();
        }
    }
    window.location.reload();
}

function FNSaveChanges(e) {

    /////// Validate Question Weight
    var wght = $('#txtValue').parent().parent().attr('id')
    var count = 0;

    if (wght.substring(2, 14) == "QuestionWght") {
        if (wght.substring(0, 2) == "SL" && wght.substring(14, 15) == "") {
            var sum = 0;
            $('.SLQuestionWeight').each(function () {
                if ($(this).text() !== "      ") {
                    sum += parseInt($(this).text());
                    count += 1;
                }
            });
            sum += parseInt($("#txtValue").val());
            count += 1;
        }
        if (wght.substring(0, 2) == "OP") {
            var sum = 0;
            $('.OPQuestionWeight').each(function () {
                if ($(this).text() !== "      ") {
                    sum += parseInt($(this).text());
                    count += 1;
                }
            });
            sum += parseInt($("#txtValue").val());
            count += 1;
        }
        if (wght.substring(0, 2) == "CO") {
            var sum = 0;
            $('.COQuestionWeight').each(function () {
                if ($(this).text() !== "      ") {
                    sum += parseInt($(this).text());
                    count += 1;
                }
            });
            sum += parseInt($("#txtValue").val());
            count += 1;
        }
        if (wght.substring(0, 2) == "FN") {
            var sum = 0;
            $('.FNQuestionWeight').each(function () {
                if ($(this).text() !== "      ") {
                    sum += parseInt($(this).text());
                    count += 1;
                }
            });
            sum += parseInt($("#txtValue").val());
            count += 1;
        }
        if (sum < 0 || sum > 100) {
            alert("Questions total Weight Should Not exceed 100");
            return;
        }

    }

    ///////// Validate Question Actual
    var scale = 100;

    var ac = $('#txtValue').parent().parent().attr('id')
    if (ac.substring(2, 14) == "QuestionAc" && ac.substring(12, 14) == "") {

        var weight = Number($('#txtValue').closest('td').prev('td').text())
        if ($('#txtValue').val() > (weight + (weight * 20 / 100))) {
            alert("Actual Should Not exceed 120% of Question Weight");
            return;
        }
    }

    ///////// Validate Question Score //////////// --------------> Only for Sales

    //var ac = $('#txtValue').parent().parent().attr('id')
    //if (ac.substring(0, 2) == "SL") {
    //    var sum = 0;
    //    $('.SLScore').each(function () {
    //        if ($(this).text() !== "      ") {
    //            sum += parseInt($(this).text());
    //        }
    //    });
    //    if (sum < 0 || sum > 120) {
    //        alert("Total Score Should Not exceed 120%");
    //        return;
    //    }
    //}

    ////////// Update Database
    
    var appraisal = {};
    appraisal.ColumnToUpdate = $("[id*=ColumnToUpdate]").val();
    appraisal.FieldValue = $("[id*=txtValue]").val();
    appraisal.AppraisalId = $("[id*=keySelected]").val();
    appraisal.QuestionId = $("[id*=keySelected2]").val();
    appraisal.EmplId = emplId;
    window.location.replace("../../AppraisalEmployee/SaveAppraisal?ColumnToUpdate=" + $("[id*=ColumnToUpdate]").val() + 
        "&txtValue=" + escape($("[id*=txtValue]").val()) + "&AppraisalId=" + $("[id*=keySelected]").val() + "&QuestionId=" + $("[id*=keySelected2]").val() +
        "&EmplId=" + emplId)
}
