﻿using Employee_Appraisal.AIFEmployee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Employee_Appraisal.Models
{
    public partial class Appraisal
    {
        public virtual AxdEntity_EmplTable_1 Employee
        {
            get
            {
                AxdEntity_EmplTable_1 employee;
                employee = AxdEntity_EmplTable_1.ConstructEmployeeById(this.EmplId, "YIAC");
                if (employee != null)
                    return employee;
                return employee = AxdEntity_EmplTable_1.ConstructEmployeeById(this.EmplId, "PAY");
            }
        }
    }
}