﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Employee_Appraisal.AIFEmployee;
using Employee_Appraisal.AIFReportingRelationships;
using Employee_Appraisal.AIFRepRel;
using System.ServiceModel.Channels;
using System.ServiceModel;
using System.Xml.Serialization;

namespace Employee_Appraisal.AIFEmployee
{
    public partial class AxdEntity_EmplTable_1
    {
        public static AxdEntity_EmplTable_1 ConstructEmployee(string login)
        {
            AxdEntity_EmplTable_1 employee;

            employee = AxdEntity_EmplTable_1.ConstructEmployee(login, "YIAC");

            if (employee.EmplId == null)
            {
                employee = AxdEntity_EmplTable_1.ConstructEmployee(login, "PAY");
            }

            //List<AxdEntity_EmplTable_1> axdEntity_EmplTable_1 = AxdEntity_EmplTable_1.ConstructEmployeeList().Where(reporting => reporting.ReportingToId == employee.EmplId).ToList();

            return employee;
        }

        public static AxdEntity_EmplTable_1 ConstructEmployee(string userIdentity, string endpoint)
        {
            string networkAlies = userIdentity.Remove(0, userIdentity.LastIndexOf('\\') + 1);

            EmplTableServiceClient client = new EmplTableServiceClient();


            MessageHeader messageHeader = MessageHeader.CreateHeader("DestinationEndpoint", "", endpoint);

            OperationContextScope o = new OperationContextScope(client.InnerChannel);

            OperationContext.Current.OutgoingMessageHeaders.Add(messageHeader);

            AIFEmployee.QueryCriteria qc = new AIFEmployee.QueryCriteria();
            AxdEmplTable emplTable = new AxdEmplTable();

            qc.CriteriaElement = new AIFEmployee.CriteriaElement[1];
            qc.CriteriaElement[0] = new AIFEmployee.CriteriaElement();
            qc.CriteriaElement[0].DataSourceName = "SysCompanyADInfo_1";
            qc.CriteriaElement[0].FieldName = "networkAlias";
            qc.CriteriaElement[0].Value1 = networkAlies;

            try
            {
                AxdEmplTable axdEmplTable = client.find(qc);

                return axdEmplTable != null ? (axdEmplTable.EmplTable_1 != null ? axdEmplTable.EmplTable_1[0] : null) : null;
            }
            catch (Exception aifException)
            {
                return null;
            }
            finally
            {
                client.Close();
            }
        }

        public static AxdEntity_EmplTable_1 ConstructEmployeeById(string EmplId, string endpoint)
        {
            EmplTableServiceClient client = new EmplTableServiceClient();


            MessageHeader messageHeader = MessageHeader.CreateHeader("DestinationEndpoint", ", endpoint);

            OperationContextScope o = new OperationContextScope(client.InnerChannel);

            OperationContext.Current.OutgoingMessageHeaders.Add(messageHeader);

            QueryCriteria qc = new QueryCriteria();
            AxdEmplTable emplTable = new AxdEmplTable();

            qc.CriteriaElement = new CriteriaElement[1];
            qc.CriteriaElement[0] = new CriteriaElement();
            qc.CriteriaElement[0].DataSourceName = "EmplTable_1";
            qc.CriteriaElement[0].FieldName = "EmplId";
            qc.CriteriaElement[0].Value1 = EmplId;

            try
            {
                AxdEmplTable axdEmplTable = client.find(qc);
                return axdEmplTable != null ? (axdEmplTable.EmplTable_1 != null ? axdEmplTable.EmplTable_1[0] : null) : null;
            }
            catch (Exception aifException)
            {
                return null;
            }
            finally
            {
                client.Close();
            }
        }

        public static List<AxdEntity_EmplTable_1> ConstructEmployeeList()
        {
            return AxdEntity_EmplTable_1.ConstructEmployeeListEndpoint("YIAC");
        }

        private static List<AxdEntity_EmplTable_1> ConstructEmployeeListEndpoint(string endpoint)
        {
            EmplTableServiceClient client = new EmplTableServiceClient();


            MessageHeader messageHeader = MessageHeader.CreateHeader("DestinationEndpoint", "", endpoint);

            OperationContextScope o = new OperationContextScope(client.InnerChannel);

            OperationContext.Current.OutgoingMessageHeaders.Add(messageHeader);

            QueryCriteria qc = new QueryCriteria();
            AxdEmplTable emplTable = new AxdEmplTable();

            qc.CriteriaElement = new CriteriaElement[1];
            qc.CriteriaElement[0] = new CriteriaElement();
            qc.CriteriaElement[0].DataSourceName = "EmplTable_1";
            qc.CriteriaElement[0].FieldName = "EmpStatus";
            qc.CriteriaElement[0].Operator = Operator.NotEqual;
            qc.CriteriaElement[0].Value1 = AxdEnum_EmpStatus.Terminated.ToString();

            try
            {
                AxdEmplTable axdEmplTable = client.find(qc);
                return axdEmplTable != null ? (axdEmplTable.EmplTable_1 != null ? axdEmplTable.EmplTable_1.ToList() : null) : null;
            }
            catch (Exception aifException)
            {
                return null;
            }
            finally
            {
                client.Close();
            }
        }

        //public string EndPoint { get; set; }

        public virtual AxdEntity_EmplTable_1 Manager
        {
            get
            {
                AxdEntity_EmplTable_1 manager;
                string managerID;

                managerID = string.IsNullOrEmpty(this.ReportingToId) ? (string.IsNullOrEmpty(this.CurrentPersonInCharge) ? null : this.CurrentPersonInCharge) : this.ReportingToId;

                manager = AxdEntity_EmplTable_1.ConstructEmployeeById(managerID, this.DataAreaId);

                return manager;
            }
        }


        //public virtual string Endpoint
        //{
        //    get
        //    {
        //        return this.endpoint;
        //    } 
        //    private set
        //    {
        //        this.endpoint = value;
        //    }
        //}

        //[XmlElement(Order = 70)]

        public List<AxdEntity_EmplTable_1> getSubordinates()
        {
            //get
            //{

            //EmplSubServiceClient client = new EmplSubServiceClient();
            EmplRepRelServiceClient client = new EmplRepRelServiceClient();

            //MessageHeader messageHeader = MessageHeader.CreateHeader("DestinationEndpoint", "", this.DataAreaId);

            OperationContextScope o = new OperationContextScope(client.InnerChannel);

            //OperationContext.Current.OutgoingMessageHeaders.Add(messageHeader);

            AIFRepRel.QueryCriteria qc = new AIFRepRel.QueryCriteria();
            //AxdEmplSub emplSub = new AxdEmplSub();
            AxdEmplRepRel emplRepRel = new AxdEmplRepRel();

            qc.CriteriaElement = new AIFRepRel.CriteriaElement[1];
            qc.CriteriaElement[0] = new AIFRepRel.CriteriaElement();
            qc.CriteriaElement[0].DataSourceName = "SysCompanyRepRel_1";
            qc.CriteriaElement[0].Operator = AIFRepRel.Operator.Equal;
            qc.CriteriaElement[0].FieldName = "ReportsTo";
            qc.CriteriaElement[0].Value1 = this.EmplId;

            //qc.CriteriaElement = new AIFReportingRelationships.CriteriaElement[5];
            //qc.CriteriaElement[0] = new AIFReportingRelationships.CriteriaElement();
            //qc.CriteriaElement[0].DataSourceName = "HRPPartyPositionTableEmpl";
            //qc.CriteriaElement[0].Operator = AIFReportingRelationships.Operator.Equal;
            //qc.CriteriaElement[0].FieldName = "Reference";
            //qc.CriteriaElement[0].Value1 = this.EmplId;

            //qc.CriteriaElement[1] = new AIFReportingRelationships.CriteriaElement();
            //qc.CriteriaElement[1].DataSourceName = "HRPPartyPositionTableEmpl";
            //qc.CriteriaElement[1].FieldName = "ValidFromDateTime";
            //qc.CriteriaElement[1].Operator = AIFReportingRelationships.Operator.LessOrEqual;
            //qc.CriteriaElement[1].Value1 = DateTime.Now.Date.ToString("yyyy-MM-ddTHH:mm:ss");

            //qc.CriteriaElement[2] = new AIFReportingRelationships.CriteriaElement();
            //qc.CriteriaElement[2].DataSourceName = "HRPPartyPositionTableEmpl";
            //qc.CriteriaElement[2].FieldName = "ValidToDateTime";
            //qc.CriteriaElement[2].Operator = AIFReportingRelationships.Operator.GreaterOrEqual;
            //qc.CriteriaElement[2].Value1 = DateTime.Now.Date.ToString("yyyy-MM-ddTHH:mm:ss");

            //qc.CriteriaElement[3] = new AIFReportingRelationships.CriteriaElement();
            //qc.CriteriaElement[3].DataSourceName = "HRPPartyPositionTableSub";
            //qc.CriteriaElement[3].FieldName = "ValidFromDateTime";
            //qc.CriteriaElement[3].Operator = AIFReportingRelationships.Operator.LessOrEqual;
            //qc.CriteriaElement[3].Value1 = DateTime.Now.Date.ToString("yyyy-MM-ddTHH:mm:ss");

            //qc.CriteriaElement[4] = new AIFReportingRelationships.CriteriaElement();
            //qc.CriteriaElement[4].DataSourceName = "HRPPartyPositionTableSub";
            //qc.CriteriaElement[4].FieldName = "ValidToDateTime";
            //qc.CriteriaElement[4].Operator = AIFReportingRelationships.Operator.GreaterOrEqual;
            //qc.CriteriaElement[4].Value1 = DateTime.Now.Date.ToString("yyyy-MM-ddTHH:mm:ss");

            try
            {
                //emplSub = client.find(qc);
                emplRepRel = client.find(qc);

                //if (emplSub != null)
                if (emplRepRel != null)
                {
                    //if (emplSub.HRPPartyPositionTableEmpl != null)
                    if (emplRepRel.SysCompanyRepRel_1 != null)
                    {
                        List<AxdEntity_EmplTable_1> _users = new List<AxdEntity_EmplTable_1>();
                        //if (emplSub.HRPPartyPositionTableEmpl[0].HRPPartyPositionTableSub != null)
                        //{
                        //for (int i = 0; i < emplSub.HRPPartyPositionTableEmpl[0].HRPPartyPositionTableSub.Count(); i++)
                        for (int i = 0; i < emplRepRel.SysCompanyRepRel_1.Count(); i++)
                            //_users.Add(ConstructEmployeeById(emplSub.HRPPartyPositionTableEmpl[0].HRPPartyPositionTableSub[i].Reference, this.DataAreaId));
                            _users.Add(ConstructEmployeeById(emplRepRel.SysCompanyRepRel_1[i].EmplId, emplRepRel.SysCompanyRepRel_1[i].LocalEndpointId));
                        return _users;
                        //}

                    }

                }
                return null;
            }
            catch (Exception aifException)
            {
                return null;
            }
            finally
            {
                client.Close();
            }
            //}
        }
    }
}